﻿/// <summary>
/// Base class for all unit commands.
/// </summary>
public abstract class UnitCommand
{
	/// <summary>
	/// Execute a command on receiver.
	/// </summary>
	/// <param name="receiver">Receiver.</param>
	public abstract void Execute (Unit reciever);
}
