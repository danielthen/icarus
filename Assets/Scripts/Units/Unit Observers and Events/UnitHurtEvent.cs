﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHurtEvent : SubjectEvent
{
    public Unit EventSender { get; set; }
    public Unit Attacker { get; set; }

    public UnitHurtEvent(Unit eventSender, Unit attacker)
    {
        EventSender = eventSender;
        Attacker = attacker;
    }
}