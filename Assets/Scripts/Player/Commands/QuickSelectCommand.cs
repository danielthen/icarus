﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickSelectCommand : PlayerCommand
{
    private QuickSelectType selectType;

    public QuickSelectCommand(QuickSelectType selectType)
    {
        this.selectType = selectType;
    }

    public override void Execute(Player reciever)
    {
        PlayerUnitManager pum = reciever.Units;
        Unit selectable = null;
        switch (selectType)
        {
            case QuickSelectType.CITYSHIP:
                selectable = pum.GetUnitByType(UnitType.CITYSHIP);
                break;
            case QuickSelectType.IDLE_WORKER:
                List<Unit> workers = pum.GetAllUnitsByType(UnitType.WORKER);
                for (int i = 0; i < workers.Count; i++)
                {
                    if (workers[i].State == UnitState.IDLE)
                    {
                        selectable = workers[i];
                        break;
                    }
                }
                break;
            case QuickSelectType.ALL_UNITS:
                if (pum.SelectedUnits.Count == 1)
                {
                    int nextSelectableUnitIndex = pum.Units.IndexOf(pum.SelectedUnits[0]) + 1;
                    selectable = pum.Units[(nextSelectableUnitIndex >= pum.Units.Count) ? 0 : nextSelectableUnitIndex];
                }
                else
                {
                    selectable = pum.Units[0];
                }
                break;
        }

        if (selectable == null) return; 

        // Select selectable and look it with player's camera.
        new SelectUnitCommand(selectable.gameObject, false).Execute(reciever);
        reciever.CameraController.SetPosition(selectable.transform.position);
    }
}
