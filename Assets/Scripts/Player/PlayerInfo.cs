﻿using UnityEngine;

/// <summary>
/// Info about player. Id, name, score, type.
/// </summary>
public class PlayerInfo
{
	public int Id { get; private set; }
	public string Name { get; private set; }
	public int Score { get; private set; }
	public Color PlayerColor { get; private set; }
    public PlayerType Type { get; private set; }

	public PlayerInfo(int id, string name, int score, Color color, PlayerType type) 
	{
		Id = id;
		Name = name;
		Score = score;
		PlayerColor = color;
        Type = type;
	}
}
