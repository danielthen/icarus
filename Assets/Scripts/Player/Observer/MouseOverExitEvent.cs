﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseOverExitEvent : SubjectEvent
{
    public Player EventSender { get; private set; }
    public MonoBehaviour MouseOverObject { get; private set; }

    public MouseOverExitEvent(Player eventSender, MonoBehaviour mouseOverObject)
    {
        EventSender = eventSender;
        MouseOverObject = mouseOverObject;
    }
}
