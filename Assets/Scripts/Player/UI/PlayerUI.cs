﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    private Animator animator;
    public enum PlayerUIElement {SOLAR_POWER, METAL, HEALTH, DEFENSE, ATTACK, NOTIFIER, POPULATION, HELPER, METAL_DECREASE, SOLAR_POWER_DECREASE, PAUSED, VICTORY, DEFEAT};

    // Resources UI
    [SerializeField] private Text solarPower;
    [SerializeField] private Text metal;
    [SerializeField] private Text population;

    // Info panel UI
    [SerializeField] private GameObject infoPanel;
    [SerializeField] private PlayerControlBarButton currentlySelectedUnit;
    [SerializeField] private Text currentlySelectedUnitHealth;
    [SerializeField] private Text currentlySelectedUnitDefense;
    [SerializeField] private Text currentlySelectedUnitAttack;
    [SerializeField] private Text solarPowerDecrease;
    [SerializeField] private Text metalDecrease;
    [SerializeField] private Image infoMessage;

    // Control panel UI
    [SerializeField] private GameObject panel;

    // Mouse panel
    [SerializeField] private GameObject mousePanel;

    // Notifier text panel
    [SerializeField] private Text notifier;
    [SerializeField] private Text helper;

    // Cursor
    public PlayerCursor PlayerCursor {get; set;}

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    /// <summary>
    /// Set GUI if game is paused or finished.
    /// </summary>
    /// <param name="state">Which state to show.</param>
    /// <param name="value">Should the paused/victory/defeat panel should be turned on or off.</param>
    public void SetInfoMessage(PlayerUIElement state, bool value)
    {
        switch (state)
        {
            case PlayerUIElement.PAUSED:
                infoMessage.sprite = Resources.Load<Sprite>("paused");
                break;
            case PlayerUIElement.VICTORY:
                infoMessage.sprite = Resources.Load<Sprite>("victory");
                break;
            case PlayerUIElement.DEFEAT:
                infoMessage.sprite = Resources.Load<Sprite>("defeat");
                break;
        }

        animator.SetBool("curtain", value);
    }

    /// <summary>
    /// Exit to menu with animation.
    /// </summary>
    public void EndGame()
    {
        animator.SetTrigger("EndGame");
    }

    /// <summary>
    /// Set player UI element text.
    /// </summary>
    /// <param name="element"></param>
    /// <param name="text"></param>
    public void SetText(PlayerUIElement element, string text)
    {
        switch(element)
        {
            case PlayerUIElement.SOLAR_POWER:
                solarPower.text = text;
                break;
            case PlayerUIElement.METAL:
                metal.text = text;
                break;
            case PlayerUIElement.HEALTH:
                currentlySelectedUnitHealth.text = text;
                break;
            case PlayerUIElement.DEFENSE:
                currentlySelectedUnitDefense.text = text;
                break;
            case PlayerUIElement.ATTACK:
                currentlySelectedUnitAttack.text = text;
                break;
            case PlayerUIElement.NOTIFIER:
                notifier.text = text;
                notifier.GetComponent<Animator>().SetTrigger("notify");
                break;
            case PlayerUIElement.POPULATION:
                population.text = text;
                break;
            case PlayerUIElement.HELPER:
                helper.text = text;
                break;
            case PlayerUIElement.METAL_DECREASE:
                metalDecrease.text = text;
                break;
            case PlayerUIElement.SOLAR_POWER_DECREASE:
                solarPowerDecrease.text = text;
                break;
        }

        if(metalDecrease.text == "" && solarPowerDecrease.text == "")
        {
            mousePanel.SetActive(false);
        }
        else
        {
            mousePanel.SetActive(true);
        }
    }

    /// <summary>
    /// Sets selected unit image sprite according to unit type in info panel UI.
    /// </summary>
    /// <param name="unitType"></param>
    public void SetUnitImageSprite(UnitType unitType)
    {
        PlayerControlBarButton pcbb = infoPanel.GetComponentInChildren<PlayerControlBarButton>();
        pcbb.Type = unitType;
        pcbb.UpdateImageSprite();
    }

    /// <summary>
    /// Acitivate or deactivate control panel.
    /// </summary>
    /// <param name="show"></param>
    public void ShowControlPanel(bool show)
    {
        panel.GetComponent<Animator>().SetBool("show", show);
    }
}
