﻿public class UnitBuildingInterruptedEvent : SubjectEvent
{
    public Unit EventSender { get; set; }
    public UnitCreationPart Part { get; set; }

    public UnitBuildingInterruptedEvent(Unit eventSender, UnitCreationPart part)
    {
        EventSender = eventSender;
        Part = part;
    }
}
