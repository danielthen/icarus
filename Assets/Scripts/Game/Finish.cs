﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    [SerializeField] private UnitType UnitTypeToFinishThisLevel;

    void OnTriggerEnter(Collider col)
    {
        Unit collidedUnit = col.GetComponent<Unit>();
        if (collidedUnit == null) return;
        if (collidedUnit.Info.Type == UnitTypeToFinishThisLevel)
        {
            Game.game.EndGame(true);
        }
    }

    void OnMouseEnter()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        humanPlayer.NotifyObservers(new MouseOverEvent(humanPlayer, this, "FINISH: move your " + UnitTypeToFinishThisLevel.ToString().ToUpper() + " here to win."));
    }

    void OnMouseExit()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        humanPlayer.NotifyObservers(new MouseOverExitEvent(humanPlayer, this));
    }
}