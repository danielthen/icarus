﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Unit movement component.
/// </summary>
public class UnitMovementComponent : SpecificUnitComponent 
{
	private NavMeshAgent agent;
    private LineRenderer pathRenderer;
    public float StoppingDistance { get; set; }

	void Start()
	{
        InitializeValues();
    }

	public override void UpdateUnit ()
    {
        if (thisUnit.State != UnitState.MOVING) { EndMoving(); return; }

        VisualizeAgentPath();

        if (!agent.pathPending && agent.remainingDistance <= StoppingDistance)
        {
            EndMoving();
        }
    }

	/// <summary>
	/// Moves to position.
	/// </summary>
	/// <param name="position">Position.</param>
	public void MoveToPosition(Vector3 position)
	{
        agent.ResetPath();
		agent.SetDestination(position);
        NeedsUpdate = true;
    }

    public void StopOrResumeAgent()
    {
        agent.isStopped = Game.game.State == Game.GameState.PAUSED;
    }

    private void EndMoving()
    {
        agent.ResetPath();
        NeedsUpdate = false;
        thisUnit.State = UnitState.IDLE;
        pathRenderer.positionCount = 0;
        thisUnit.NotifyObservers(new CommandExecutedEvent(thisUnit.Commander));
    }
    
    /// <summary>
    /// Draw path.
    /// </summary>
    private void VisualizeAgentPath()
    {
        if (agent.pathPending) return;
        NavMeshPath path = agent.path;
        Vector3[] positions = path.corners;
        pathRenderer.positionCount = 2;
        pathRenderer.SetPosition(0, positions[0]);
        pathRenderer.SetPosition(1, positions[positions.Length - 1]);
    }

    public void UpdateUnitSpeed(float update)
    {
        agent.speed += update;
        agent.acceleration = agent.speed * 2f;
    }

    private void InitializeValues()
    {
        thisUnit = GetComponent<Unit>();
        agent = gameObject.AddComponent<NavMeshAgent>();
        StoppingDistance = 5f;
        NeedsUpdate = false;
        agent.stoppingDistance = 1f;

        pathRenderer = gameObject.AddComponent<LineRenderer>();
        pathRenderer.material = Resources.Load<Material>("PlayerColor");
        pathRenderer.material.color = thisUnit.GetPlayer().Info.PlayerColor;

        switch (thisUnit.Info.Type)
        {
            case UnitType.CITYSHIP:
                agent.radius = 20f;
                agent.angularSpeed = 0f;
                agent.speed = 10f;
                break;
            case UnitType.FIGHTER:
                agent.radius = 5f;
                agent.angularSpeed = 250f;
                agent.speed = 250f;
                break;
            case UnitType.BATTLESHIP:
                agent.radius = 10f;
                agent.angularSpeed = 20f;
                agent.speed = 20f;
                break;
            case UnitType.WORKER:
                agent.radius = 5f;
                agent.angularSpeed = 100f;
                agent.speed = 100f;
                break;
            default:
                break;
        }
        agent.acceleration = agent.speed * 2f;
    }
}
