﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControlBarButton : MonoBehaviour
{
    public UnitType Type {get; set;}

    void Start()
    {
        UpdateImageSprite();
    }

    /// <summary>
    /// Change image according to control bar button type.
    /// </summary>
    /// <param name="image"></param>
    /// <param name="path"></param>
    private void ChangeImageAccordingToType(Image image, string path)
    {
        Sprite sprite = Resources.Load<Sprite>(path);
        image.sprite = sprite;
    }

    /// <summary>
    /// Updates image sprite.
    /// </summary>
    public void UpdateImageSprite()
    {
        Image image = GetComponentsInChildren<Image>()[1];

        switch (Type)
        {
            case UnitType.CITYSHIP:
                ChangeImageAccordingToType(image, "cityship_icon");
                break;
            case UnitType.FIGHTER:
                ChangeImageAccordingToType(image, "fighter_icon");
                break;
            case UnitType.BATTLESHIP:
                ChangeImageAccordingToType(image, "battleship_icon");
                break;
            case UnitType.WORKER:
                ChangeImageAccordingToType(image, "worker_icon");
                break;
            case UnitType.DOCK:
                ChangeImageAccordingToType(image, "icon_dock");
                break;
            case UnitType.WEAPON:
                ChangeImageAccordingToType(image, "icon_weapon");
                break;
            case UnitType.ENGINE:
                ChangeImageAccordingToType(image, "icon_engine");
                break;
            case UnitType.TOWER:
                ChangeImageAccordingToType(image, "icon_tower");
                break;
            default:
                break;
        }
    }
}