﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player visual effects.
/// </summary>
public class PlayerVisualEffects : MonoBehaviour 
{
    private PlayerCursor mouseCursor;
	private GameObject clickIndicator;

	void Start()
	{
        Cursor.visible = false;
		clickIndicator = Instantiate (Resources.Load ("ClickIndicator") as GameObject, transform.position, Quaternion.identity);
		clickIndicator.transform.parent = gameObject.transform;
        mouseCursor = GetComponent<Player>().UserInterface.PlayerCursor;
	}

    void FixedUpdate()
    {
        // Update player mouse cursor graphics position.
        mouseCursor.transform.position = Vector3.Lerp(mouseCursor.transform.position, Input.mousePosition, 0.9f);
    }

	/// <summary>
	/// Animates the click indicator at position.
	/// </summary>
	/// <param name="position">Position.</param>
	/// <param name="state">Animation type.</param>
	public void AnimateClickIndicatorAtPosition(Vector3 position, string state)
	{
		clickIndicator.transform.position = position;
		clickIndicator.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
		clickIndicator.GetComponent<Animator> ().SetTrigger (state);
	}
}
