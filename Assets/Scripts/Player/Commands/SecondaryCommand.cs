﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Secondary command: animates player's click and adds commands to selected units.
/// </summary>
public class SecondaryCommand : PlayerCommand
{
	// Target to interact on.
	private GameObject target;

	// Position where interaction needs to be happen.
	private Vector3 position;

	// Add command to queue or execute it immediately.
	private bool addCommandToQueue;

	public SecondaryCommand(GameObject target, Vector3 position, bool addCommandToQueue)
	{
		this.target = target;
		this.position = position;
		this.addCommandToQueue = addCommandToQueue;
	}

	public override void Execute (Player player)
	{
		// Act according to target.
		switch (target.layer) 
		{
            // Case building platform.
            case 8:
                if (!player.Units.HasUnitWithSpecificTypeSelected(UnitType.CITYSHIP)) return;
                BuildablePart bp = target.GetComponent<BuildablePart>();
                if (bp == null) return;
                player.Sounds.PlayAudio(PlayerAudio.AudioType.BUILD_COMMAND);
                player.Units.AddCommandToSpecificUnitType(new BuildCommand(player, bp), addCommandToQueue, UnitType.CITYSHIP);
                player.Visuals.AnimateClickIndicatorAtPosition(target.transform.position, "GatherClick");
                break;
            // Case ground.
            case 9:
                player.Sounds.PlayAudio(PlayerAudio.AudioType.MOVE_COMMAND);
                player.Units.AddCommandToSelectedUnits (new MoveCommand (position, MoveCommandType.DEFAULT, new List<Unit>(player.Units.SelectedUnits)), addCommandToQueue);
                player.Visuals.AnimateClickIndicatorAtPosition(position, "MoveClick");
                break;
			// Case unit.
			case 10:
                player.Sounds.PlayAudio(PlayerAudio.AudioType.ATTACK_COMMAND);
                player.Units.AddCommandToSelectedUnits(new MoveCommand(target.transform.position, MoveCommandType.ATTACK, new List<Unit>(player.Units.SelectedUnits)), addCommandToQueue);
                player.Units.AddCommandToSelectedUnits (new AttackCommand (target.GetComponent<Unit>()), true);
                player.Visuals.AnimateClickIndicatorAtPosition(target.transform.position, "AttackClip");
                break;
			// Case resource.
			case 11:
                player.Sounds.PlayAudio(PlayerAudio.AudioType.GATHER_COMMAND);
                Resource resource = target.GetComponent<Resource>();
                player.Units.AddCommandToSelectedUnits(new MoveCommand(target.transform, MoveCommandType.GATHER, new List<Unit>(player.Units.SelectedUnits)), addCommandToQueue);
                player.Units.AddCommandToSelectedUnits (new GatherCommand(resource, player), true);
                player.Visuals.AnimateClickIndicatorAtPosition(target.transform.position, "GatherClick");
                break;
		}
	}
}
