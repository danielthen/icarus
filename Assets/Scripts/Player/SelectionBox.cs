﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionBox
{
    /*
     * [1]
     * Implementation based on a tutorial by Jeff Zimmer.
     * https://hyunkell.com/blog/rts-style-unit-selection-in-unity-5/
     * Watched on 24.03.2019
     */
 
    public Vector3 StartPositon { get; set; }

    // Implementation based on a tutorial by Jeff Zimmer [1].
    private Texture2D texture;

    // Implementation based on a tutorial by Jeff Zimmer [1].
    private Texture2D Texture
    {
        get
        {
            if (texture == null)
            {
                texture = new Texture2D(1, 1);
                texture.SetPixel(0, 0, Color.white);
                texture.Apply();
            }

            return texture;
        }
    }

    /// <summary>
    /// Draw selection box.
    /// Implementation based on a tutorial by Jeff Zimmer [1].
    /// </summary>
    /// <param name="color"></param>
    /// <param name="endPosition"></param>
    /// <param name="thickness"></param>
    public void Draw(Color color, Vector3 endPosition, float thickness)
    {
        GUI.color = color;

        Vector2 pos1 = new Vector2(StartPositon.x, Screen.height - StartPositon.y);
        Vector2 pos2 = new Vector2(endPosition.x, Screen.height - endPosition.y);

        Vector2 minPos = Vector2.Min(pos1, pos2);
        Vector2 maxPos = Vector2.Max(pos1, pos2);

        Rect rect = Rect.MinMaxRect(minPos.x, minPos.y, maxPos.x, maxPos.y);

        GUI.DrawTexture(new Rect(rect.xMin, rect.yMin, rect.width, thickness), Texture);
        GUI.DrawTexture(new Rect(rect.xMin, rect.yMin, thickness, rect.height), Texture);
        GUI.DrawTexture(new Rect(rect.xMax - thickness, rect.yMin, thickness, rect.height), Texture);
        GUI.DrawTexture(new Rect(rect.xMin, rect.yMax - thickness, rect.width, thickness), Texture);
    }

    /// <summary>
    /// Get selection bounds.
    /// Implementation based on a tutorial by Jeff Zimmer [1].
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="endPosition"></param>
    /// <returns>Selection bounds</returns>
    private Bounds GetSelectionBoxBounds(Camera camera, Vector3 endPosition)
    {
        Bounds bounds = new Bounds();

        var view1 = camera.ScreenToViewportPoint(StartPositon);
        var view2 = camera.ScreenToViewportPoint(endPosition);

        var min = Vector3.Min(view1, view2);
        var max = Vector3.Max(view1, view2);
        min.z = camera.nearClipPlane;
        max.z = camera.farClipPlane;

        bounds.SetMinMax(min, max);

        return bounds;
    }

    /// <summary>
    /// Is unit within selection bounds.
    /// Implementation based on a tutorial by Jeff Zimmer [1].
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="unit"></param>
    /// <param name="endPosition"></param>
    /// <returns>True if it is - otherwise false.</returns>
    public bool IsWithinSelectionBounds(Camera camera, Unit unit, Vector3 endPosition)
    {
        Bounds bounds = GetSelectionBoxBounds(camera, endPosition);
        return bounds.Contains(camera.WorldToViewportPoint(unit.transform.position));
    }
}