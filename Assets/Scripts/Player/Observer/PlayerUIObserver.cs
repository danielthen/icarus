﻿using UnityEngine;

public class PlayerUIObserver : Observer
{
    Sprite[] secondaryCommandMouseSprites = Resources.LoadAll<Sprite>("secondary_commands");

    public override void OnNotify(SubjectEvent subEvent)
    {
        if (subEvent is ResourceChangeEvent)
        {
            ResourceChangeEvent rge = subEvent as ResourceChangeEvent;
            PlayerUI ui = rge.EventSender.UserInterface;
            PlayerResources resources = rge.EventSender.AllResources;

            // 1. Update resources value in player's resources information script and
            // 2. on GUI.
            switch (rge.TypeOfResource)
            {
                case ResourceType.METAL:
                    resources.Metal += rge.ResourceChangeAmount;
                    ui.SetText(PlayerUI.PlayerUIElement.METAL, "" + resources.Metal);
                    break;
                case ResourceType.SOLAR_POWER:
                    resources.SolarPower += rge.ResourceChangeAmount;
                    ui.SetText(PlayerUI.PlayerUIElement.SOLAR_POWER, "" + resources.SolarPower);
                    break;
            }
        }
        else if (subEvent is MouseOverEvent)
        {
            MouseOverEvent moe = subEvent as MouseOverEvent;
            PlayerUI ui = moe.EventSender.UserInterface;
            MonoBehaviour mb = moe.MouseOverObject;

            // This event mainly changes second command indicator icon on mouse pointer.
            ui.SetText(PlayerUI.PlayerUIElement.HELPER, moe.HelperText);

            if (mb is Unit)
            {
                PlayerUnitManager pum = moe.EventSender.Units;
                if (pum.HasUnitWithSpecificTypeSelected(UnitType.BATTLESHIP) 
                    || pum.HasUnitWithSpecificTypeSelected(UnitType.FIGHTER)
                    || (pum.HasUnitWithSpecificTypeSelected(UnitType.CITYSHIP) && pum.CityShipHasWeapons))
                {
                    ui.PlayerCursor.ChangeSecondaryCommandIndicator(secondaryCommandMouseSprites[1]);
                }
            }
            else if (mb is Resource)
            {
                if (!moe.EventSender.Units.HasUnitWithSpecificTypeSelected(UnitType.WORKER)) return;
                switch ((mb as Resource).Type)
                {
                    case ResourceType.METAL:
                        ui.PlayerCursor.ChangeSecondaryCommandIndicator(secondaryCommandMouseSprites[4]);
                        break;
                    case ResourceType.SOLAR_POWER:
                        ui.PlayerCursor.ChangeSecondaryCommandIndicator(secondaryCommandMouseSprites[3]);
                        break;
                }
            }
            else if (mb is BuildablePart)
            {
                BuildablePart bp = mb as BuildablePart;
                if (bp.HasBuilding) return;
                if (moe.EventSender.Units.HasUnitWithSpecificTypeSelected(UnitType.CITYSHIP))
                {
                    ui.PlayerCursor.ShowSecondaryCommandIndicator(true);
                    ui.PlayerCursor.ChangeSecondaryCommandIndicator(secondaryCommandMouseSprites[0]);
                    Vector2 cost = bp.Cost;
                    ui.SetText(PlayerUI.PlayerUIElement.SOLAR_POWER_DECREASE, "-" + cost.x);
                    ui.SetText(PlayerUI.PlayerUIElement.METAL_DECREASE, "-" + cost.y);
                }
            }
            else if (mb is WorldGrid)
            {
                ui.PlayerCursor.ChangeSecondaryCommandIndicator(secondaryCommandMouseSprites[2]);
            }
            else if (mb is CreatablePart)
            {
                CreatablePart cp = mb as CreatablePart;
                cp.Highlight(true);
                Vector2 cost = cp.Cost;
                ui.SetText(PlayerUI.PlayerUIElement.SOLAR_POWER_DECREASE, "-" + cost.x);
                ui.SetText(PlayerUI.PlayerUIElement.METAL_DECREASE, "-" + cost.y);
            }
        }
        else if (subEvent is UnitSelectionEvent)
        {
            UnitSelectionEvent use = subEvent as UnitSelectionEvent;
            PlayerUI ui = use.EventSender.UserInterface;

            // If some units are selected then help player by showing him/her icons what commands he/she can execute.

            if(use.EventSender.Units.SelectedUnitsCount > 0)
            {
                if (use.EventSender.Units.HasUnitWithSecondayCommand())
                {
                    ui.PlayerCursor.ShowSecondaryCommandIndicator(true);
                }
                
            }
            else
            {
                ui.PlayerCursor.ShowSecondaryCommandIndicator(false);
            }
        }
        else if (subEvent is MouseOverExitEvent)
        {
            MouseOverExitEvent moe = subEvent as MouseOverExitEvent;
            PlayerUI ui = moe.EventSender.UserInterface;
            MonoBehaviour mb = moe.MouseOverObject;

            ui.SetText(PlayerUI.PlayerUIElement.HELPER, "");

            if (mb is BuildablePart)
            {
                if (moe.EventSender.Units.SelectedUnitsCount <= 0 || !moe.EventSender.Units.HasUnitWithSecondayCommand())
                {
                    ui.PlayerCursor.ShowSecondaryCommandIndicator(false);
                }
                ui.SetText(PlayerUI.PlayerUIElement.SOLAR_POWER_DECREASE, "");
                ui.SetText(PlayerUI.PlayerUIElement.METAL_DECREASE, "");
            }
            else if (mb is CreatablePart)
            {
                (mb as CreatablePart).Highlight(false);
                ui.SetText(PlayerUI.PlayerUIElement.SOLAR_POWER_DECREASE, "");
                ui.SetText(PlayerUI.PlayerUIElement.METAL_DECREASE, "");
            }
        }
        else if (subEvent is PopulationChangeEvent)
        {
            PopulationChangeEvent pce = subEvent as PopulationChangeEvent;
            PlayerUI ui = pce.EventSender.UserInterface;

            // Update changes made in population in GUI.
            // Also update these changes in player's population instance.
            pce.EventSender.Population.CurrentPopulation += pce.PopulationChangeAmount;
            pce.EventSender.Population.MaxPopulation += pce.PopulationCapacityChangeAmount;
            ui.SetText(PlayerUI.PlayerUIElement.POPULATION, pce.EventSender.Population.CurrentPopulation + "/" + pce.EventSender.Population.MaxPopulation);
            ui.SetText(PlayerUI.PlayerUIElement.NOTIFIER, "UNIT CREATED.");
        }
        else if (subEvent is PauseEvent)
        {
            PauseEvent pe = subEvent as PauseEvent;
            pe.EventSender.UserInterface.SetInfoMessage(PlayerUI.PlayerUIElement.PAUSED, Game.game.State == Game.GameState.PAUSED);
            PlayerUnitManager pum = pe.EventSender.Units;

            // Notify also all currently moving units to stop or resume.
            foreach(Unit u in pum.Units)
            {
                UnitMovementComponent umc = u.GetComponent<UnitMovementComponent>();
                if (umc != null)
                {
                    umc.StopOrResumeAgent();
                }
            }
        }
        else if (subEvent is VictoryEvent)
        {
            VictoryEvent ve = subEvent as VictoryEvent;
            ve.EventSender.Sounds.PlayAudio(PlayerAudio.AudioType.ALARM);
            ve.EventSender.UserInterface.SetInfoMessage(PlayerUI.PlayerUIElement.VICTORY, true);
        }
        else if (subEvent is DefeatEvent)
        {
            DefeatEvent de = subEvent as DefeatEvent;
            de.EventSender.UserInterface.SetInfoMessage(PlayerUI.PlayerUIElement.DEFEAT, true);
        }
        else if (subEvent is NotEnoughResourcesEvent)
        {
            NotEnoughResourcesEvent nere = subEvent as NotEnoughResourcesEvent;
            nere.UserInterface.SetText(PlayerUI.PlayerUIElement.NOTIFIER, "NOT ENOUGH OF RESOURCES TO CREATE ANOTHER UNIT.");
        }
        else if (subEvent is NotEnoughPopulationEvent)
        {
            NotEnoughPopulationEvent nepe = subEvent as NotEnoughPopulationEvent;
            nepe.UserInterface.SetText(PlayerUI.PlayerUIElement.NOTIFIER, "NOT ENOUGH OF POPULATION FOR ANTOHER UNIT.");
        }
        else if (subEvent is UnderAttackEvent)
        {
            UnderAttackEvent uae = subEvent as UnderAttackEvent;
            uae.PlayerGUI.SetText(PlayerUI.PlayerUIElement.NOTIFIER, "YOU ARE UNDER ATTACK!");
        }
        else if (subEvent is DangerZoneExpandEvent)
        {
            DangerZoneExpandEvent dzee = subEvent as DangerZoneExpandEvent;
            dzee.EventSender.UserInterface.SetText(PlayerUI.PlayerUIElement.NOTIFIER, "DANGER ZONES EXPANDING!");
            dzee.EventSender.Sounds.PlayAudio(PlayerAudio.AudioType.ALARM);
        }
        else if (subEvent is EnemyAttackEvent)
        {
            EnemyAttackEvent eae = subEvent as EnemyAttackEvent;
            eae.EventSender.UserInterface.SetText(PlayerUI.PlayerUIElement.NOTIFIER, "ENEMIES ARE COMING!");
            eae.EventSender.Sounds.PlayAudio(PlayerAudio.AudioType.ALARM);
        }
    }
}