﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public Player PlayerWhoShotThisBullet { get; set; }
    public float DamageAmount { get; set; }
    public Unit UnitWhoShotThisBomb { get; set; }
    public Unit Target { get; set; }

    void Start()
    {
        GetComponent<AudioSource>().pitch = Random.Range(0.95f, 1.1f);
    }

    void OnTriggerEnter(Collider col)
    {
        Unit hitUnit = col.gameObject.GetComponent<Unit>();
        if (hitUnit == null) return;
        if (hitUnit == UnitWhoShotThisBomb) return;
        if (Target == null) return;
        if (hitUnit != Target) return;

        hitUnit.Stats.Damage(DamageAmount);
        GameObject explosion = Instantiate(Resources.Load<GameObject>("Explosion"), col.transform.position, Quaternion.identity);
        explosion.GetComponent<AudioSource>().pitch = Random.Range(0.95f, 1.1f);
        hitUnit.Stats.Damage(DamageAmount);
        Destroy(explosion, 1f);
        Destroy(gameObject);
        hitUnit.NotifyObservers(new UnitHurtEvent(hitUnit, UnitWhoShotThisBomb));
    }
}