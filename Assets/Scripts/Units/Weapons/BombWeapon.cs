﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombWeapon : UnitWeapon
{
    private GameObject bullet;

    void Start()
    {
        bullet = Resources.Load<GameObject>("Bomb");
    }

    public override void Shoot(Transform transform, Unit target)
    {
        GameObject go = Instantiate(bullet, transform.position, transform.rotation);
        Bomb bomb = go.GetComponent<Bomb>();
        bomb.Target = target;
        bomb.PlayerWhoShotThisBullet = UnitThatOwnsThisWeapon.GetPlayer();
        bomb.DamageAmount = UnitThatOwnsThisWeapon.GetComponent<UnitBattleComponent>().DamageValue;
        bomb.UnitWhoShotThisBomb = UnitThatOwnsThisWeapon;
        go.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * 200f, ForceMode.Impulse);
        Destroy(go, 10f);
    }
}
