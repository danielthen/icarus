﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Observer. Observer design pattern gameprogrammingpatterns.com/observer.html
/// </summary>
public abstract class Observer
{
	/// <summary>
	/// Raises the notify event.
	/// </summary>
	/// <param name="subEvent">Subject event.</param>
	public abstract void OnNotify(SubjectEvent subEvent);
}
