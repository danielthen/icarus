﻿public class PauseEvent : SubjectEvent
{
    public Player EventSender { get; set; }

    public PauseEvent(Player eventSender)
    {
        EventSender = eventSender;
    }
}
