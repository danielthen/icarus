﻿using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Node for priority queue.
/// </summary>
public class PriorityNode<T> 
{
	public T Content {get; set;}
	public float Value {get; set;}

	public PriorityNode(T content, float value)
	{
		this.Content = content;
		this.Value = value;
	}
}
