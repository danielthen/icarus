﻿public enum UnitState
{
    IDLE,
    MOVING,
    GATHERING,
    ATTACKING,
    BUILDING,
    PREPARING
}