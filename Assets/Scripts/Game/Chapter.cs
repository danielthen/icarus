﻿using UnityEngine;
using UnityEngine.UI;

public class Chapter : MonoBehaviour
{
    private Menu menu;
    [SerializeField] private int chapterNumber;
    [SerializeField] private string chapterInfo;

    private Animator animator;
    [SerializeField] private Text infoText;

    void Start()
    {
        menu = FindObjectOfType<Menu>();
        animator = GetComponent<Animator>();
    }

    void OnMouseDown()
    {
        animator.SetTrigger("Play");
    }

    void OnMouseOver()
    {
        if (menu.Started) return;
        menu.SelectedLevel = chapterNumber;
        animator.SetBool("showInfo", true);
        infoText.text = "Chapter " + chapterNumber + " - " + chapterInfo;
    }

    void OnMouseExit()
    {
        if (menu.Started) return;
        animator.SetBool("showInfo", false);
        infoText.text = "";
    }
}
