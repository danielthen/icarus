﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackEvent : SubjectEvent
{
    public Player EventSender { get; set; }

    public EnemyAttackEvent(Player eventSender)
    {
        EventSender = eventSender;
    }
}
