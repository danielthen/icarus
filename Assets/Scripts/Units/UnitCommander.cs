﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Unit's commands holder and executer.
/// </summary>
public class UnitCommander : MonoBehaviour 
{
    private Unit thisUnit;

	// All comands unit must execute.
	private Queue<UnitCommand> commands;
    public bool HasMoreCommandsToExecute {get { return commands.Count > 0; } }

	void Start()
	{
        thisUnit = GetComponent<Unit>();
		commands = new Queue<UnitCommand>();
	}

	/// <summary>
	/// Adds the command to commands queue.
	/// </summary>
	/// <param name="command">Command.</param>
	public void AddCommandToQueue(UnitCommand command)
	{
        if(thisUnit.State == UnitState.IDLE)
        {
            command.Execute(thisUnit);
            return;
        }
        commands.Enqueue (command);
	}

	/// <summary>
	/// Executes a command immediately.
	/// Also clears commands queue.
	/// </summary>
	/// <param name="command">Command.</param>
	public void AddCommand(UnitCommand command)
	{
		commands.Clear();
		command.Execute(thisUnit);
	}

	/// <summary>
	/// Executes one command from commands stack.
	/// </summary>
	public void ExecuteOneCommand()
	{
		commands.Dequeue().Execute(thisUnit);
	}
    
    /// <summary>
    /// Clear all commands.
    /// </summary>
    public void ClearCommands()
    {
        commands.Clear();

        thisUnit.State = UnitState.IDLE;
    }
}