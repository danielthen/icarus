﻿using UnityEngine;

public class DockGUI : MonoBehaviour
{
    public CreatablePart[] CreatableParts { get; private set; }

    void Start()
    {
        CreatableParts = GetComponentsInChildren<CreatablePart>();
    }

    /// <summary>
    /// Show or hide all creatable parts.
    /// </summary>
    /// <param name="show"></param>
    public void ShowAllCreatableParts(bool show)
    {
        foreach(CreatablePart cp in CreatableParts)
        {
            cp.Show(show);
        }
    }

    /// <summary>
    /// Show progress how much time is stil needed to create a unit on a specific creatable part.
    /// </summary>
    /// <param name="creatablePart">Part to update.</param>
    /// <param name="currentValue">Current building progress.</param>
    /// <param name="maxValue">Maximum time to build unit.</param>
    public void ShowProgress(CreatablePart creatablePart, float currentValue, float maxValue)
    {
        creatablePart.Icon.fillAmount = 1 - (currentValue / maxValue);
    }

    /// <summary>
    /// Reset progress icon.
    /// </summary>
    public void ResetProgress(CreatablePart creatablePart)
    {
        creatablePart.Icon.fillAmount = 1f;
    }
}