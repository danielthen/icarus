﻿public class UnitStateChangeObserver : Observer
{
    public override void OnNotify(SubjectEvent subEvent)
    {
        if (!(subEvent is UnitStateChangeEvent)) return;

        UnitStateChangeEvent usce = subEvent as UnitStateChangeEvent;

        switch(usce.OldState)
        {
            case UnitState.GATHERING:
                break;
            case UnitState.ATTACKING:
                break;
            case UnitState.BUILDING:
                break;
            case UnitState.IDLE:
                break;
            case UnitState.MOVING:
                break;
        }
    }
}
