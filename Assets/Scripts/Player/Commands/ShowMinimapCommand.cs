﻿public class ShowMinimapCommand : PlayerCommand
{
    public override void Execute(Player player)
    {
        player.CameraController.ShowMinimap();
    }
}
