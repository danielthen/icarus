﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Unit info about its name and type.
/// </summary>
public class UnitInfo : MonoBehaviour
{
	public int Id {get; set;}

	[SerializeField] private string _name;
	[SerializeField] private UnitType _type;

	public string Name {get { return _name;} set {_name = value;}}
	public UnitType Type {get { return _type;} set {_type = value;}}
}
