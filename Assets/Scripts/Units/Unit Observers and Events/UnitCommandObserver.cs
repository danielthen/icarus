﻿public class UnitCommandObserver : Observer
{
    public override void OnNotify(SubjectEvent subEvent)
    {
        if(subEvent is CommandExecutedEvent)
        {
            CommandExecutedEvent cee = subEvent as CommandExecutedEvent;
            if (cee.Commander.HasMoreCommandsToExecute)
            {
                cee.Commander.ExecuteOneCommand();
            }
        }
    }
}