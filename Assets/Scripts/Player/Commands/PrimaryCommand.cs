﻿using UnityEngine;

public class PrimaryCommand : PlayerCommand
{
    // Target to interact on.
    private GameObject target;

    // Add command to queue or execute it immediately.
    private bool addCommandToQueue;

    public PrimaryCommand(GameObject target, bool addCommandToQueue)
    {
        this.target = target;
        this.addCommandToQueue = addCommandToQueue;
    }

    public override void Execute(Player player)
    {
        // Act according to target.
        switch (target.layer)
        {
            // Case creatable unit part.
            case 8:
                UnitCreationPart ucp = target.GetComponent<UnitCreationPart>();
                if (ucp == null) return;

                if (ucp is CreatablePart)
                {
                    player.Units.AddCommandToSelectedUnits(new BuildCommand(player, ucp as CreatablePart), true);
                }
                else if (ucp is BuildablePart)
                {
                    new SelectUnitCommand(player.Units.GetUnitByType(UnitType.CITYSHIP).gameObject, false).Execute(player);
                }
                
                break;
            default:
                new SelectUnitCommand(target, addCommandToQueue).Execute(player);
                break;
        }
    }
}
