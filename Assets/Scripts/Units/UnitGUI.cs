﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitGUI : MonoBehaviour
{
    public HealthBar UnitHealthBar { get; private set; }

    void Start()
    {
        UnitHealthBar = GetComponentInChildren<HealthBar>();
    }
}