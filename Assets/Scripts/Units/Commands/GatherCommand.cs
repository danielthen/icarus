﻿using UnityEngine;

public class GatherCommand : UnitCommand
{
	private Resource resource;
    private Player sender;

	public GatherCommand(Resource resource, Player sender)
	{
		this.resource = resource;
        this.sender = sender;
	}

	public override void Execute(Unit reciever)
	{
        UnitGatherComponent ugc = reciever.GetComponent<UnitGatherComponent>();

        if (ugc == null) { reciever.NotifyObservers(new CommandExecutedEvent(reciever.Commander)); return; }

        reciever.State = UnitState.GATHERING;

        if(ugc.ThisUnitPlayer == null)
        {
            ugc.ThisUnitPlayer = sender;
        }

        ugc.ResourceToCollectFrom = resource;
        ugc.StartGathering();
    }
}