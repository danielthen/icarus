﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    public enum AudioType { SELECT_UNIT, ATTACK_COMMAND, MOVE_COMMAND, GATHER_COMMAND, BUILD_COMMAND, ALARM}
    private AudioSource[] playerAudios;

    void Start()
    {
        playerAudios = new AudioSource[11];
        for (int i = 0; i < 11; i++)
        {
            playerAudios[i] = gameObject.AddComponent<AudioSource>();
            playerAudios[i].playOnAwake = false;
            playerAudios[i].volume = 0.3f;
        }
    }

    /// <summary>
    /// Play audio according to audio type.
    /// </summary>
    /// <param name="type"></param>
    public void PlayAudio(AudioType type)
    {
        AudioSource playerAudio = FindInactiveAudioPlayer();
        playerAudio.pitch = Random.Range(0.95f, 1.05f);

        switch (type)
        {
            case AudioType.SELECT_UNIT:
                playerAudio.clip = Resources.Load<AudioClip>("Sounds/click");
                break;
            case AudioType.ATTACK_COMMAND:
                playerAudio.clip = Resources.Load<AudioClip>("Sounds/Sing4");
                break;
            case AudioType.MOVE_COMMAND:
                playerAudio.clip = Resources.Load<AudioClip>("Sounds/Sing3");
                break;
            case AudioType.GATHER_COMMAND:
                playerAudio.clip = Resources.Load<AudioClip>("Sounds/Sing2");
                break;
            case AudioType.BUILD_COMMAND:
                playerAudio.clip = Resources.Load<AudioClip>("Sounds/Sing1");
                break;
            case AudioType.ALARM:
                playerAudios[10].clip = Resources.Load<AudioClip>("Sounds/Sing5");
                playerAudios[10].Play();
                return;
        }

        playerAudio.Play();
    }

    private AudioSource FindInactiveAudioPlayer()
    {
        for (int i = 0; i < playerAudios.Length - 1; i++)
        {
            if (!playerAudios[i].isPlaying)
            {
                return playerAudios[i];
            }
        }

        return playerAudios[0];
    }
}
