﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityshipObserver : Observer
{
    public override void OnNotify(SubjectEvent subEvent)
    {
        if (subEvent is CityshipUnitCreated)
        {
            CityshipUnitCreated cuc = subEvent as CityshipUnitCreated;
            switch (cuc.CreatedUnit.Info.Type)
            {
                case UnitType.DOCK:
                    Player player = cuc.EventSender.GetPlayer();
                    player.NotifyObservers(new PopulationChangeEvent(player, 0, 15));
                    break;
                case UnitType.ENGINE:
                    UnitMovementComponent umc = cuc.EventSender.GetComponent<UnitMovementComponent>();
                    if(umc != null)
                    {
                        umc.UpdateUnitSpeed(20f);
                    }
                    break;
                case UnitType.TOWER:
                    cuc.EventSender.Components.Add(cuc.EventSender.gameObject.AddComponent<UnitMovementComponent>());
                    break;
                case UnitType.WEAPON:
                    break;
            }
        }
        else if (subEvent is CityshipUnitDestroyed)
        {
            CityshipUnitDestroyed cuc = subEvent as CityshipUnitDestroyed;
            Player player = cuc.EventSender.GetPlayer();

            switch (cuc.DestroyedUnit.Info.Type)
            {
                case UnitType.DOCK:
                    player.NotifyObservers(new PopulationChangeEvent(player, 0, -15));
                    break;
                case UnitType.ENGINE:
                    UnitMovementComponent umc = cuc.EventSender.GetComponent<UnitMovementComponent>();
                    if (umc != null)
                    {
                        umc.UpdateUnitSpeed(-20f);
                    }
                    break;
                case UnitType.TOWER:
                    foreach(SpecificUnitComponent specUnComponent in cuc.EventSender.Components)
                    {
                        if(specUnComponent is UnitMovementComponent)
                        {
                            cuc.EventSender.Components.Remove(specUnComponent);
                        }
                    }
                    break;
                case UnitType.WEAPON:
                    break;
            }

            cuc.Part.HasBuilding = false;
        }
    }
}
