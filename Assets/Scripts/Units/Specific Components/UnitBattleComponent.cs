﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Unit component for fighting.
/// </summary>
public class UnitBattleComponent : SpecificUnitComponent
{
    private Unit target;
    private List<UnitWeapon> weapons;
    private float currentActionTime;
    private float battleCooldown;
    public float DamageValue { get; set; }

    void Start()
    {
        InitializeValues();
    }

    public override void UpdateUnit()
    {
        if (target == null) { EndAttacking(); return; }

        if (Vector3.Distance(transform.position, target.transform.position) > thisUnit.Stats.ActionRange) { EndAttackingButTryToMoveTowardsTargetAgain(); return; }

        currentActionTime += Time.deltaTime;
        
        if (currentActionTime < battleCooldown) return;

        Attack();

        currentActionTime = 0f;
    }

    /// <summary>
    /// Shoot with every weapon the ship has.
    /// </summary>
    private void Attack()
    {
        foreach (UnitWeapon uw in weapons)
        {
            uw.transform.LookAt(target.transform.position);
            uw.Shoot(uw.transform, target);
        }
    }

    /// <summary>
    /// Start attacking a target.
    /// </summary>
    /// <param name="target"></param>
    public void StartAttacking(Unit target)
    {
        this.target = target;
        // First time when unit starts attacking then let it attack after 0.5 seconds.
        currentActionTime = battleCooldown - 0.5f;
        NeedsUpdate = true;
    }

    /// <summary>
    /// End battle.
    /// </summary>
    private void EndAttacking()
    {
        if(thisUnit.State == UnitState.ATTACKING) thisUnit.State = UnitState.IDLE;
        NeedsUpdate = false;
        thisUnit.NotifyObservers(new CommandExecutedEvent(thisUnit.Commander));
    }

    /// <summary>
    /// End battle but try to move towards target.
    /// </summary>
    private void EndAttackingButTryToMoveTowardsTargetAgain()
    {
        if (thisUnit.State == UnitState.ATTACKING)  thisUnit.State = UnitState.IDLE;
        NeedsUpdate = false;
        if (target == null) return;
        thisUnit.Commander.AddCommandToQueue(new MoveCommand(target.transform.position, MoveCommandType.ATTACK, new List<Unit> { thisUnit }));
        thisUnit.NotifyObservers(new CommandExecutedEvent(thisUnit.Commander));
    }

    public void InitializeValues()
    {
        thisUnit = GetComponent<Unit>();
        currentActionTime = 0f;
        NeedsUpdate = false;
        weapons = new List<UnitWeapon>();
        foreach (UnitWeapon uw in GetComponentsInChildren<UnitWeapon>())
        {
            uw.UnitThatOwnsThisWeapon = thisUnit;
            weapons.Add(uw);
        }

        switch (thisUnit.Info.Type)
        {
            case UnitType.FIGHTER:
                DamageValue = 2f;
                battleCooldown = 0.5f;
                break;
            case UnitType.BATTLESHIP:
                DamageValue = 10f;
                battleCooldown = 2f;
                break;
            case UnitType.WEAPON:
                DamageValue = 50f;
                battleCooldown = 5f;
                break;
            default:
                break;
        }
    }
}