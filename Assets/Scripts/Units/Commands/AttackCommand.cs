﻿public class AttackCommand : UnitCommand 
{
	private Unit target;

	public AttackCommand(Unit target) 
	{
		this.target = target;
	}

	public override void Execute(Unit reciever)
	{
        UnitBattleComponent ubc = reciever.GetComponent<UnitBattleComponent>();

        if (ubc == null) { reciever.NotifyObservers(new CommandExecutedEvent(reciever.Commander)); return; }

        reciever.State = UnitState.ATTACKING;

        if (reciever.Info.Type == UnitType.CITYSHIP)
        {
            foreach (Unit building in reciever.GetPlayer().Units.CityshipUnits)
            {
                if (building.Info == null) return;
                if(building.Info.Type == UnitType.WEAPON)
                {
                    building.GetComponent<UnitBattleComponent>().StartAttacking(target);
                }
            }

            return;
        }
        else
        {
            ubc.StartAttacking(target);
        }
    }
}
