﻿public class DefeatEvent : SubjectEvent
{
    public Player EventSender { get; set; }

    public DefeatEvent(Player eventSender)
    {
        EventSender = eventSender;
    }
}
