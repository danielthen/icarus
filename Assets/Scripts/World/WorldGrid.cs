﻿using UnityEngine;

/// <summary>
/// World grid to represent the world.
/// </summary>
public class WorldGrid : MonoBehaviour 
{
    /*
	// Grid size
	[SerializeField] private float gridWorldWidth;
	[SerializeField] private float gridWorldHeight;
	private int gridRowCount;
	private int gridColumnCount;

	// Nodes
	[SerializeField] private float nodeRadius;
	private WorldNode[,] nodes;

	//Plane
	private Transform ground;


	void Start()
	{
		CreateGrid ();
	}

	/// <summary>
	/// Calculates all required values for grid and creates it.
	/// </summary>
	private void CreateGrid()
	{
		float nodeDiameter = nodeRadius * 2;
		gridRowCount = Mathf.RoundToInt (gridWorldWidth / nodeDiameter);
		gridColumnCount = Mathf.RoundToInt (gridWorldHeight / nodeDiameter);

		nodes = new WorldNode[gridRowCount, gridColumnCount];

		// First we get top left of grid world position.
		Vector3 worldTopLeftPosition = new Vector3 (transform.position.x - gridWorldWidth / 2, transform.position.y, transform.position.z - gridWorldHeight / 2);

		// Then we will calculate node position in the world.
		for (int i = 0; i < gridRowCount; i++)
		{
			for (int j = 0; j < gridColumnCount; j++) 
			{
				Vector3 nodePosition = new Vector3 (worldTopLeftPosition.x + nodeDiameter * (i + 1) + nodeRadius, worldTopLeftPosition.y, worldTopLeftPosition.z + nodeDiameter * (j + 1) + nodeRadius);
				nodes[i, j] = new WorldNode (i, j, nodePosition);
			}
		}
	}*/

    void OnMouseEnter()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        humanPlayer.NotifyObservers(new MouseOverEvent(humanPlayer, this, ""));
    }

    void OnMouseExit()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        humanPlayer.NotifyObservers(new MouseOverExitEvent(humanPlayer, this));
    }
}