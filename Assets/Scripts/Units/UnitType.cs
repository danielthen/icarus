﻿public enum UnitType
{
    CITYSHIP,
    FIGHTER,
    BATTLESHIP,
    WORKER,
    DOCK,
    WEAPON,
    ENGINE,
    TOWER
}