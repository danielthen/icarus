﻿using System.Collections.Generic;

public class UnitSelectionEvent : SubjectEvent 
{
    public Player EventSender { get; set; }
	public List<Unit> SelectedUnits { get; set; }
    public List<Unit> DeselectedUnits { get; set; }

	public UnitSelectionEvent(Player eventSender, List<Unit> selectedUnits, List<Unit> deselectedUnits)
	{
        EventSender = eventSender;
		SelectedUnits = selectedUnits;
        DeselectedUnits = deselectedUnits;
	}
}
