﻿using UnityEngine;

/// <summary>
/// Singleton for a game.
/// </summary>
public class Game : MonoBehaviour
{
    public enum GameState { IDLE, OVER, PAUSED }

    // All players of one game.
    private static Player[] players;

    public GameState State { get; set; }

    private static Game myGame;

    public static Game game
    {
        get
        {
            if (myGame == null)
            {
                GameObject go = new GameObject();
                myGame = go.AddComponent<Game>();
                players = FindObjectsOfType<Player>();
                myGame.State = GameState.IDLE;
            }

            return myGame;
        }
    }

    public Player HumanPlayer
    {
        get
        {
            foreach (Player p in players)
            {
                if (p.Info.Type == PlayerType.HUMAN)
                {
                    return p;
                }
            }

            return null;
        }
    }

    /// <summary>
    /// End game.
    /// </summary>
    /// <param name="victorious">Is player victorious or not.</param>
    public void EndGame(bool victorious)
    {
        Player humanPlayer = HumanPlayer;

        if (victorious)
        {
            humanPlayer.NotifyObservers(new VictoryEvent(humanPlayer));
        }
        else
        {
            humanPlayer.NotifyObservers(new DefeatEvent(humanPlayer));
        }

        State = GameState.OVER;
    }
}
