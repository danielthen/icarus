﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Observer subject (subscriber).
/// </summary>
public class Subject : MonoBehaviour
{
	protected List<Observer> observers;

	/// <summary>
	/// Adds the observer to observers list.
	/// </summary>
	/// <param name="observer">Observer.</param>
	protected void AddObserver(Observer observer) 
	{
		observers.Add (observer);
	}

	/// <summary>
	/// Removes the observer from observers list.
	/// </summary>
	/// <param name="observer">Observer.</param>
	protected void RemoveObserver(Observer observer) 
	{
		observers.Add (observer);
	}

	/// <summary>
	/// Notifies all observers in the observers list with subject event.
	/// </summary>
	/// <param name="subjectEvent">Subject event.</param>
	public void NotifyObservers(SubjectEvent subjectEvent)
	{
		foreach (Observer o in observers) 
		{
			o.OnNotify (subjectEvent);
		}
	}
}
