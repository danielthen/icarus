﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Observe if unit was selected.
/// </summary>
public class UnitSelectionObserver : Observer
{
	public override void OnNotify (SubjectEvent subEvent)
	{
		if (!(subEvent is UnitSelectionEvent)) return;

        UnitSelectionEvent use = subEvent as UnitSelectionEvent;
        PlayerUI ui = use.EventSender.UserInterface;
        PlayerUnitManager pum = use.EventSender.Units;

        ui.ShowControlPanel(false);

        // 1. Turn selection indicators on/off.
        SetActiveUnitsChildren(use.DeselectedUnits, "Selection", false);
        SetActiveUnitsChildren(use.SelectedUnits, "Selection", true);

        //2. Update GUI with currently selected unit.
        if (pum.SelectedUnitsCount <= 0) return;
        Unit firstSelected = pum.GetSelectedUnitByIndex(0);

        ui.SetText(PlayerUI.PlayerUIElement.ATTACK, ((firstSelected.GetComponent<UnitBattleComponent>() != null) ? firstSelected.GetComponent<UnitBattleComponent>().DamageValue : 0) + "");
        ui.SetText(PlayerUI.PlayerUIElement.DEFENSE, firstSelected.Stats.Armour + "");
        ui.SetText(PlayerUI.PlayerUIElement.HEALTH, Mathf.Round(firstSelected.Stats.Health) + "");
        ui.SetUnitImageSprite(firstSelected.Info.Type);
        ui.ShowControlPanel(true);
	}

    /// <summary>
    /// Activate or deactivate units children.
    /// For example used for activating/deactivating units selection indicator.
    /// </summary>
    /// <param name="units"></param>
    /// <param name="tag"></param>
    /// <param name="active"></param>
    private void SetActiveUnitsChildren(List<Unit> units, string tag, bool active)
    {
        foreach (Unit u in units)
        {
            for (int i = 0; i < u.transform.childCount; i++)
            {
                if (u.transform.GetChild(i).tag == tag)
                {
                    u.transform.GetChild(i).gameObject.SetActive(active);
                    if(active)
                    {
                        u.GraphicalUserInterface.UnitHealthBar.Show(true);
                    }
                    else
                    {
                        u.GraphicalUserInterface.UnitHealthBar.Show(false);
                    }
                    break;
                }

                // Also show or hide dock GUI.
                if (u.Info.Type == UnitType.DOCK)
                {
                    u.GetComponent<DockGUI>().ShowAllCreatableParts(active);
                }
            }
        }
    }
}
