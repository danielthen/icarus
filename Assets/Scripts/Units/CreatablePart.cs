﻿using UnityEngine;
using UnityEngine.UI;

public class CreatablePart : UnitCreationPart
{
    private Animator animator;
    public Image Icon { get; set; }

    void Start()
    {
        animator = GetComponent<Animator>();
        Icon = GetComponent<Image>();
        BuildingLocation = transform.parent.parent.parent.GetChild(0);
    }

    /// <summary>
    /// Show or hide this element.
    /// </summary>
    /// <param name="show"></param>
    public void Show(bool show)
    {
        animator.SetBool("show", show);
    }

    /// <summary>
    /// Highlight or not this element.
    /// </summary>
    /// <param name=""></param>
    public void Highlight(bool highlight)
    {
        animator.SetBool("highlight", highlight);
    }

    void OnMouseEnter()
    {
        Player player = Game.game.HumanPlayer;
        player.NotifyObservers(new MouseOverEvent(player, this, OnMouseOverFeedback()));
    }

    void OnMouseExit()
    {
        Player player = Game.game.HumanPlayer;
        player.NotifyObservers(new MouseOverExitEvent(player, this));
    }

    /// <summary>
    /// Return correct feedback according to creatable unit type.
    /// </summary>
    private string OnMouseOverFeedback()
    {
        switch (Type)
        {
            case UnitType.FIGHTER:
                return "Create a FIGHTER that is FAST and FIGHTS with enemies but is WEAK."; 
            case UnitType.BATTLESHIP:
                return "Create a BATTLESHIP that is STRONG and FIGHTS with enemies but is SLOW.";
            case UnitType.WORKER:
                return "Create a WORKER that GATHERS resources.";
        }

        return "";
    }
}
