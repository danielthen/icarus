﻿using UnityEngine;

/// <summary>
/// Unit specific component.
/// For example movement, gathering, battling, etc.
/// </summary>
public abstract class SpecificUnitComponent : MonoBehaviour 
{
	public abstract void UpdateUnit ();
	public bool NeedsUpdate { get; set; }
    protected Unit thisUnit;
}
