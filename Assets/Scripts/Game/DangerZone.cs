﻿using UnityEngine;
using System.Collections.Generic;

public class DangerZone : MonoBehaviour
{
    private enum DangerZoneState { EXPANDING, COOLDOWN }
    private GameObject explosion;
    [SerializeField] private bool expandable;
    [SerializeField] private float expandModifier;
    private DangerZoneState state;
    [SerializeField] private float timeAfterChangeState;
    private float currentTime;
    private List<Unit> unitsToDamage;
    private float currentDamageTime;

    void Start()
    {
        state = DangerZoneState.COOLDOWN;
        explosion = Resources.Load<GameObject>("Explosion");
        currentTime = 0f;
        unitsToDamage = new List<Unit>();
        currentDamageTime = 0f;
    }

    void Update()
    {
        if (Game.game.State == Game.GameState.PAUSED || Game.game.State == Game.GameState.OVER) return;

        currentDamageTime += Time.deltaTime;

        if (currentDamageTime > 1f)
        {
            DamageAllUnitsInDangerZone();
            currentDamageTime = 0f;
        }

        if (!expandable) return;

        currentTime += Time.deltaTime;

        // State change.
        if (currentTime > timeAfterChangeState)
        {
            currentTime = 0f;
            switch (state)
            {
                case DangerZoneState.EXPANDING:
                    state = DangerZoneState.COOLDOWN;
                    break;
                case DangerZoneState.COOLDOWN:
                    Player player = Game.game.HumanPlayer;
                    player.NotifyObservers(new DangerZoneExpandEvent(player));
                    state = DangerZoneState.EXPANDING;
                    break;
            }
        }

        // Act according to state.
        switch (state)
        {
            case DangerZoneState.EXPANDING:
                Expand();
                break;
            case DangerZoneState.COOLDOWN:
                break;
        }
    }

    private void Expand()
    {
        transform.localScale += new Vector3(1f, 0f, 1f) * expandModifier * Time.deltaTime;
    }

    void OnTriggerEnter(Collider col)
    {
        Unit collidedUnit = col.GetComponent<Unit>();
        if (collidedUnit == null) return;
        unitsToDamage.Add(collidedUnit);
    }

    void OnTriggerExit(Collider col)
    {
        Unit collidedUnit = col.GetComponent<Unit>();
        if (collidedUnit == null) return;
        unitsToDamage.Remove(collidedUnit);
    }

    void OnMouseEnter()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        humanPlayer.NotifyObservers(new MouseOverEvent(humanPlayer, this, "DANGER ZONE: It EXPANDS over time. AVOID it with your UNITS or they shall be DESTROYED."));
    }

    void OnMouseExit()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        humanPlayer.NotifyObservers(new MouseOverExitEvent(humanPlayer, this));
    }

    private void DamageAllUnitsInDangerZone()
    {
        for (int i = 0; i < unitsToDamage.Count; i++)
        {
            Unit u = unitsToDamage[i];

            if(u == null)
            {
                unitsToDamage.RemoveAt(i);
                continue;
            }

            u.Stats.Damage(u.Stats.MaxHealth * 0.1f);
            Instantiate(explosion, u.transform.position, Quaternion.identity);
            u.NotifyObservers(new UnitHurtEvent(u, null));
        }
    }
}
