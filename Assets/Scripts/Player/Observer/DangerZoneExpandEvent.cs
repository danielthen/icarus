﻿public class DangerZoneExpandEvent : SubjectEvent
{
    public Player EventSender { get; set; }

    public DangerZoneExpandEvent(Player eventSender)
    {
        EventSender = eventSender;
    }
}
