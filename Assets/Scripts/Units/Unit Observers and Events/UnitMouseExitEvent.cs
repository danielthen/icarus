﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMouseExitEvent : SubjectEvent
{
    public Unit ThisUnit { get; set; }

    public UnitMouseExitEvent(Unit thisUnit)
    {
        ThisUnit = thisUnit;
    }
}
