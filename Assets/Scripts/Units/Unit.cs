﻿using System;
using System.Collections.Generic;

/// <summary>
/// Base class for a unit.
/// </summary>
public class Unit : Subject
{
	public UnitInfo Info { get; set; }
	public UnitStats Stats { get; set; }
	public UnitCommander Commander { get; set; }
    public UnitGUI GraphicalUserInterface { get; set; }
	public List<SpecificUnitComponent> Components { get; set; }
    private UnitState state;
    public UnitState State
    {
        get { return state; }
        set
        {
            // Notify observers if state has changed.
            NotifyObservers(new UnitStateChangeEvent(this, state, value));
            state = value;
        }
    }

	private void Start()
	{
        // Add observers.
        observers = new List<Observer>
        {
            new UnitStateChangeObserver(),
            new UnitCommandObserver(),
            new UnitGUIObserver()
        };

        State = UnitState.IDLE;
		Info = GetComponent<UnitInfo> ();
		Stats = GetComponent<UnitStats> ();
		Commander = gameObject.AddComponent<UnitCommander>();
		Components = new List<SpecificUnitComponent> ();
        GraphicalUserInterface = gameObject.GetComponentInChildren<UnitGUI>();

        // Add specific unit components accordingly to unit type.
        switch (Info.Type)
        {
            case UnitType.CITYSHIP:
                Components.Add(gameObject.AddComponent<UnitBuildingComponent>());
                Components.Add(gameObject.AddComponent<UnitBattleComponent>());
                observers.Add(new CityshipObserver());
                break;
            case UnitType.BATTLESHIP:
                Components.Add(gameObject.AddComponent<UnitMovementComponent>());
                Components.Add(gameObject.AddComponent<UnitBattleComponent>());
                break;
            case UnitType.FIGHTER:
                Components.Add(gameObject.AddComponent<UnitMovementComponent>());
                Components.Add(gameObject.AddComponent<UnitBattleComponent>());
                break;
            case UnitType.WORKER:
                Components.Add(gameObject.AddComponent<UnitMovementComponent>());
                Components.Add(gameObject.AddComponent<UnitGatherComponent>());
                break;
            case UnitType.DOCK:
                CityshipUnitNotification();
                Components.Add(gameObject.AddComponent<UnitBuildingComponent>());
                gameObject.AddComponent<DockGUI>();
                break;
            case UnitType.TOWER:
                CityshipUnitNotification();
                break;
            case UnitType.WEAPON:
                CityshipUnitNotification();
                Components.Add(gameObject.AddComponent<UnitBattleComponent>());
                break;
            case UnitType.ENGINE:
                CityshipUnitNotification();
                break;
        }
    }

    /// <summary>
    /// Gets unit player.
    /// </summary>
    /// <returns>Player if it exists - otherwise null.</returns>
    public Player GetPlayer()
    {
        return GetComponentInParent<Player>();
    }

    void OnMouseEnter()
    {
        NotifyObservers(new UnitMouseOverEvent(this));

        Player humanPlayer = Game.game.HumanPlayer;

        // Also we want to notify player observers that mouse is over this unit.
        humanPlayer.NotifyObservers(new MouseOverEvent(humanPlayer, this, Info.Name.ToUpper()));
    }

    void OnMouseExit()
    {
        NotifyObservers(new UnitMouseExitEvent(this));

        Player humanPlayer = Game.game.HumanPlayer;

        // Also we want to notify player observers that mouse is not over this unit anymore.
        humanPlayer.NotifyObservers(new MouseOverExitEvent(humanPlayer, this));
    }

    /// <summary>
    /// Notify cityship if cityship unit is created.
    /// </summary>
    private void CityshipUnitNotification()
    {
        try
        {
            Unit cityship = GetPlayer().Units.GetUnitByType(UnitType.CITYSHIP);
            cityship.NotifyObservers(new CityshipUnitCreated(cityship, this));
        }
        catch (NullReferenceException) { }
    }
}
