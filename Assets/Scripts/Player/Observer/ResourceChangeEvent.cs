﻿public class ResourceChangeEvent : SubjectEvent
{
    public Player EventSender {get; private set;}
    public int ResourceChangeAmount {get; private set;}
    public ResourceType TypeOfResource {get; private set;}

    public ResourceChangeEvent(Player eventSender, int resourceChangeAmount, ResourceType resourceType)
    {
        EventSender = eventSender;
        ResourceChangeAmount = resourceChangeAmount;
        TypeOfResource = resourceType;
    }
}