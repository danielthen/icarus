﻿public class UnitBuildingFinishedEvent : SubjectEvent
{
    public Unit EventSender { get; set; }
    public UnitCreationPart Part { get; set; }

    public UnitBuildingFinishedEvent(Unit eventSender, UnitCreationPart part)
    {
        EventSender = eventSender;
        Part = part;
    }
}
