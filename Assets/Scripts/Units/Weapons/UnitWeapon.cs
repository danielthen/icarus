﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitWeapon : MonoBehaviour
{
    public abstract void Shoot(Transform transform, Unit target);
    public Unit UnitThatOwnsThisWeapon { get; set; }
}
