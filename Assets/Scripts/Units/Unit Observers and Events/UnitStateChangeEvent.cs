﻿public class UnitStateChangeEvent : SubjectEvent
{
    public Unit Sender { get; set; }
    public UnitState OldState { get; set; }
    public UnitState NewState { get; set; }

    public UnitStateChangeEvent(Unit sender, UnitState oldState, UnitState newState)
    {
        Sender = sender;
        OldState = oldState;
        NewState = newState;
    }
}
