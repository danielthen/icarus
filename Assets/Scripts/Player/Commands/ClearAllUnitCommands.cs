﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearAllUnitCommands : PlayerCommand
{
    public override void Execute(Player player)
    {
        player.Units.ClearAllSelectedUnitCommands();
    }
}
