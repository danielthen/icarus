﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player controller and information holder.
/// </summary>
public class Player : Subject
{
    [SerializeField] private int _id;
    [SerializeField] private string _name;
    [SerializeField] private Color _color;
    [SerializeField] private PlayerType _type;

	public PlayerInfo Info { get; set; }
	public PlayerResources AllResources { get; set; }
    public PlayerPopulation Population { get; set; }
	public PlayerUnitManager Units { get; set; }
	public PlayerControl Control { get; set; }
	public PlayerVisualEffects Visuals { get; set; }
	public PlayerCameraController CameraController { get; set; }
    public PlayerUI UserInterface { get; set; }
    public PlayerAudio Sounds { get; set; }

	void Start()
	{
        observers = new List<Observer>();
        
        //Evaluate info id, name ...
        Info = new PlayerInfo(_id, name, 0, _color, _type);
        
        // Resources component initialize.
        AllResources = new PlayerResources();

        // Population component initialize.
        Population = new PlayerPopulation();

        // Units component initialize.
        Units = gameObject.AddComponent<PlayerUnitManager>();

        switch (Info.Type)
        {
            case PlayerType.HUMAN:
                // Player control component initialize.
                Control = gameObject.AddComponent<HumanPlayerController>();

                UserInterface = GetComponentInChildren<PlayerUI>();
                UserInterface.PlayerCursor = UserInterface.gameObject.GetComponentInChildren<PlayerCursor>();

                // Visual component initialize.
                Visuals = gameObject.AddComponent<PlayerVisualEffects>();

                // Player camera initialize.
                GameObject playerCamera = new GameObject("PlayerCamera");
                playerCamera.transform.parent = transform;
                CameraController = playerCamera.gameObject.AddComponent<PlayerCameraController>();

                Sounds = gameObject.AddComponent<PlayerAudio>();

                // Add observers.
                observers.Add(new PlayerUIObserver());
                observers.Add(new UnitSelectionObserver());

                // Add starting resources and population.
                NotifyObservers(new ResourceChangeEvent(this, 600, ResourceType.METAL));
                NotifyObservers(new ResourceChangeEvent(this, 350, ResourceType.SOLAR_POWER));
                NotifyObservers(new PopulationChangeEvent(this, 0, 25));
                break;
            case PlayerType.COMPUTER:
                // Player control component initialize.
                Control = gameObject.AddComponent<AIPlayerController>();
                (Control as AIPlayerController).MaxUnitCount = 1;
                (Control as AIPlayerController).Cooldown = 25f;
                break;
        }
    }

    /*void OnTriggerEnter(Collider col)
    {
        if (Info.Type == PlayerType.HUMAN) return;
        Unit u = col.GetComponent<Unit>();
        if (u == null) return;
        if (u.Info.Type != UnitType.CITYSHIP) return;
        (Control as AIPlayerController).ConcentrateOnCityship();
    }*/
}
