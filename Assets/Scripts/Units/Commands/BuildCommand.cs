﻿public class BuildCommand : UnitCommand
{
    private UnitCreationPart unitCreationPart;
    private Player sender;

    public BuildCommand(Player sender, UnitCreationPart unitCreationPart)
    {
        this.sender = sender;
        this.unitCreationPart = unitCreationPart;
    }

    public override void Execute(Unit reciever)
    {
        UnitBuildingComponent ubc = reciever.GetComponent<UnitBuildingComponent>();

        if (ubc == null) return;
        
        if(sender.AllResources.SolarPower < unitCreationPart.Cost.x || sender.AllResources.Metal < unitCreationPart.Cost.y)
        {
            sender.NotifyObservers(new NotEnoughResourcesEvent(sender.UserInterface));
            return;
        }

        if (unitCreationPart is CreatablePart)
        {
            if(sender.Population.CurrentPopulation + 1 > sender.Population.MaxPopulation)
            {
                sender.NotifyObservers(new NotEnoughPopulationEvent(sender.UserInterface));
                return;
            }
        }

        if (unitCreationPart is BuildablePart)
        {
            if ((unitCreationPart as BuildablePart).HasBuilding) return;
        }

        reciever.State = UnitState.BUILDING;

        ubc.StartBuilding(unitCreationPart);
    }
}
