﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnderAttackEvent : SubjectEvent
{
    public PlayerUI PlayerGUI { get; set; }
    
    public UnderAttackEvent(PlayerUI playerGUI)
    {
        PlayerGUI = playerGUI;
    }
}
