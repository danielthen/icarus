﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildablePartMouseOverEvent : SubjectEvent
{
    public BuildablePart EventSender { get; set; }
    public UnitBuildingPanel BuildingPanel { get; set; }

    public BuildablePartMouseOverEvent(BuildablePart eventSender, UnitBuildingPanel buildingPanel)
    {
        EventSender = eventSender;
        BuildingPanel = buildingPanel;
    }
}