﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandExecutedEvent : SubjectEvent
{
    public UnitCommander Commander { get; set; }

    public CommandExecutedEvent(UnitCommander commander)
    {
        Commander = commander;
    }
}
