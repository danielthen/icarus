﻿using UnityEngine;

public class PlayerCameraController : MonoBehaviour 
{
	public Camera MyCamera {get; set;}
	public float followSpeed;

    private Vector3 currentPosition;
    private Vector3 oldPosition;
    private bool minimapShown;

    [SerializeField] private float maxY;
    [SerializeField] private float minY;

	void Awake()
	{
		MyCamera = gameObject.AddComponent<Camera>();
        MyCamera.backgroundColor = Color.black;
		transform.rotation = Quaternion.Euler (45f, 0f, 0f);
        MyCamera.farClipPlane = 10000f;
        MyCamera.cullingMask = ~(1 << LayerMask.NameToLayer("Icon"));

        currentPosition.x = transform.parent.position.x;
        currentPosition.y = 240f;
        currentPosition.z = transform.parent.position.z - 240f;

        MyCamera.gameObject.AddComponent<AudioListener>();

		transform.position = currentPosition;

		followSpeed = 25f;

        minY = 14f;
        maxY = 1000f;
        minimapShown = false;
	}

    /// <summary>
    /// Update camera position.
    /// </summary>
	public void UpdateCamera()
	{
		SetPosition ();
		UpdatePosition ();
        MoveWithMouse();
	}

	/// <summary>
	/// Updates the position.
	/// </summary>
	private void UpdatePosition()
	{
		transform.position = Vector3.Lerp(transform.position, currentPosition, followSpeed * Time.deltaTime);
    }

	/// <summary>
	/// Sets the position camera to follow.
	/// </summary>
	/// <returns>The position.</returns>
	private void SetPosition()
	{
		//Vector3 mousePos = MyCamera.ViewportToWorldPoint(Input.mousePosition);
        if(minimapShown)
        {
            currentPosition.z += Input.GetAxis("Vertical") * 30000f * Time.deltaTime;
            currentPosition.x += Input.GetAxis("Horizontal") * 30000f * Time.deltaTime;
        }
        else
        {
            float zChange = Input.GetAxis("ScrollWheel") * 25000f * Time.deltaTime;
            float yChange = -Input.GetAxis("ScrollWheel") * 25000f * Time.deltaTime;

            if (currentPosition.y + yChange > minY && currentPosition.y + yChange < maxY)
            {
                currentPosition.z += zChange;
                currentPosition.y += yChange;
            }

            currentPosition.z += Input.GetAxis("Vertical") * 2500f * Time.deltaTime;
            currentPosition.x += Input.GetAxis("Horizontal") * 2500f * Time.deltaTime;
        }

		//currentX = mousePos.x;
		//currentZ = mousePos.z;
	}

    /// <summary>
    /// Set camera x and z coordinate.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    public void SetPosition(Vector3 vector)
    {
        if (minimapShown)
        {
            currentPosition.x = vector.x;
            currentPosition.z = vector.z - oldPosition.y;
            currentPosition.y = oldPosition.y;
            minimapShown = false;
            transform.rotation = Quaternion.Euler(45f, 0f, 0f);
            MyCamera.clearFlags = CameraClearFlags.Skybox;
            MyCamera.cullingMask = ~(1 << LayerMask.NameToLayer("Icon"));
            return;
        }
        else
        {
            currentPosition.x = vector.x;
            currentPosition.z = vector.z - currentPosition.y;
        }
        
    }

    /// <summary>
    /// Show minimap.
    /// </summary>
    public void ShowMinimap()
    {
        if(minimapShown)
        {
            minimapShown = false;
            currentPosition = oldPosition;
            transform.rotation = Quaternion.Euler(45f, 0f, 0f);
            //MyCamera.clearFlags = CameraClearFlags.Skybox;
            MyCamera.cullingMask = ~(1 << LayerMask.NameToLayer("Icon"));
            return;
        }

        oldPosition = currentPosition;
        currentPosition = new Vector3(0f, 8000f, 0f);
        transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        MyCamera.cullingMask = (1 << LayerMask.NameToLayer("Icon"));
        //MyCamera.clearFlags = CameraClearFlags.Color;
        minimapShown = true;
    }

    public void MoveWithMouse()
    {
        Vector3 mousePosition = MyCamera.ScreenToViewportPoint(Input.mousePosition);

        if (mousePosition.x < 0.1f)
        {
            currentPosition.x += -1000f * Time.deltaTime;
        }
        else if (mousePosition.x > 0.9f)
        {
            currentPosition.x += 1000f * Time.deltaTime;
        }

        if (mousePosition.y < 0.1f)
        {
            currentPosition.z += -1000f * Time.deltaTime;
        }
        else if(mousePosition.y > 0.9f)
        {
            currentPosition.z += 1000f * Time.deltaTime;
        }

    }
}
