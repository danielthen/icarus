﻿public class CityshipUnitCreated : SubjectEvent
{
    public Unit EventSender { get; set; }
    public Unit CreatedUnit { get; set; }

    public CityshipUnitCreated(Unit eventSender, Unit createdUnit)
    {
        EventSender = eventSender;
        CreatedUnit = createdUnit;
    }
}
