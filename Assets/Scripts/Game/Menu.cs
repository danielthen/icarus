﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public bool Started { get; set; }
    private Animator animator;
    public int SelectedLevel { get; set; }

    void Start()
    {
        Cursor.visible = true;
        animator = GetComponent<Animator>();
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Play()
    {
        Started = true;
        animator.SetTrigger("Play");
    }
}
