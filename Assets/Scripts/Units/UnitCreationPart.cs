﻿using UnityEngine;

public class UnitCreationPart : MonoBehaviour
{
    [SerializeField] private UnitType _type;
    public UnitType Type { get { return _type; } }
    public Transform BuildingLocation { get; set; }
    public Vector2 Cost
    {
        get
        {
            switch (_type)
            {
                case UnitType.CITYSHIP:
                    return new Vector2(1000f, 1000f);
                case UnitType.FIGHTER:
                    return new Vector2(100f, 100f);
                case UnitType.BATTLESHIP:
                    return new Vector2(150f, 150f);
                case UnitType.WORKER:
                    return new Vector2(50f, 50f);
                case UnitType.DOCK:
                    return new Vector2(250f, 500f);
                case UnitType.WEAPON:
                    return new Vector2(200f, 700f);
                case UnitType.ENGINE:
                    return new Vector2(700f, 500f);
                case UnitType.TOWER:
                    return new Vector2(1500f, 2000f);
            }

            return Vector2.zero;
        }
    }
}
