﻿using UnityEngine;
using UnityEngine.EventSystems;

public class HumanPlayerController : PlayerControl
{
    private Player player;
    private PlayerCameraController playerCamera;

    // Selection box.
    private SelectionBox selectionBox;
    private bool boxSelecting;

    void Start()
    {
        selectionBox = new SelectionBox();
        player = GetComponent<Player>();
        playerCamera = player.CameraController;
        boxSelecting = false;
    }

    void Update()
    {
        DoAccordingToUserInput();
    }

    /// <summary>
    /// Act according to user input.
    /// </summary>
    private void DoAccordingToUserInput()
    {
        if (Input.GetButtonDown("Jump") && Game.game.State == Game.GameState.OVER)
        {
            AddKeyCommand(KeyCommandType.EXIT);
        }

        if (Game.game.State == Game.GameState.OVER) return;

        if (Input.GetButtonDown("Cancel"))
        {
            AddKeyCommand(KeyCommandType.PAUSE);
        }
        else if (Input.GetButtonDown("Jump") && Game.game.State == Game.GameState.PAUSED)
        {
            AddKeyCommand(KeyCommandType.EXIT);
        }

        if (Game.game.State == Game.GameState.PAUSED) return;

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            AddMouseCommand(0, Input.GetButton("Multiple"));
            boxSelecting = true;
            selectionBox.StartPositon = Input.mousePosition;
        }
        else if (Input.GetMouseButtonDown(1) && !EventSystem.current.IsPointerOverGameObject())
        {
            AddMouseCommand(1, Input.GetButton("Multiple"));
        }
        else if (Input.GetMouseButtonUp(0))
        {
            boxSelecting = false;
        }
        else if (Input.GetMouseButtonDown(2) && !EventSystem.current.IsPointerOverGameObject())
        {
            AddMouseCommand(2, false);
        }

        if (Input.GetButtonDown("Jump"))
        {
            AddKeyCommand(KeyCommandType.SHOW_MINIMAP);
        }
        else if (Input.GetButtonDown("Clear_Commands"))
        {
            AddKeyCommand(KeyCommandType.CLEAR_ALL_UNIT_COMMANDS);
        }
        else if (Input.GetButtonDown("Select_Cityship"))
        {
            AddKeyCommand(KeyCommandType.QUICK_SELECT_CITYSHIP);
        }
        else if (Input.GetButtonDown("Select_All_Units"))
        {
            AddKeyCommand(KeyCommandType.QUICK_SELECT_BETWEEN_ALL_UNITS);
        }
        else if (Input.GetButtonDown("Select_Idle_Workers"))
        {
            AddKeyCommand(KeyCommandType.QUICK_SELECT_IDLE_WORKER);
        }

        // Box selection.
        if (boxSelecting)
        {
            foreach (Unit u in player.Units.Units)
            {
                if (player.Units.IsSelected(u)) continue;

                if (selectionBox.IsWithinSelectionBounds(playerCamera.MyCamera, u, Input.mousePosition))
                {
                    new SelectUnitCommand(u.gameObject, true).Execute(player);
                }
            }
        }

        playerCamera.UpdateCamera();
    }

    /// <summary>
    /// When player clicks somewhere it adds a command to player commands.
    /// </summary>
    /// <param name="mouseButtonInput">Mouse button input number.</param>
    /// <param name="addCommandToQueue">Is control button being pressed - should we add it to commands queue when giving it to a unit or multiple selection feature.</param>
    private void AddMouseCommand(int mouseButtonInput, bool addCommandToQueue)
    {
        // Shoot a ray.
        Ray mouseRay = player.CameraController.MyCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(mouseRay, out hit))
        {
            switch (mouseButtonInput)
            {
                // Left mouse button.
                case 0:
                    if (hit.collider.gameObject != null)
                    {
                        new PrimaryCommand(hit.collider.gameObject, addCommandToQueue).Execute(player);
                    }

                    break;
                // Right mouse button.
                case 1:
                    new SecondaryCommand(hit.collider.gameObject, hit.point, addCommandToQueue).Execute(player);
                    break;
                // Middle mouse button.
                case 2:
                    new MoveCameraCommand(hit.point).Execute(player);
                    break;
            }
        }
    }

    /// <summary>
    /// User key press commands.
    /// </summary>
    /// <param name="commandType"></param>
    private void AddKeyCommand(KeyCommandType commandType)
    {
        switch (commandType)
        {
            case KeyCommandType.SHOW_MINIMAP:
                new ShowMinimapCommand().Execute(player);
                break;
            case KeyCommandType.PAUSE:
                new PauseCommand().Execute(player);
                break;
            case KeyCommandType.CLEAR_ALL_UNIT_COMMANDS:
                new ClearAllUnitCommands().Execute(player);
                break;
            case KeyCommandType.EXIT:
                new ExitCommand().Execute(player);
                break;
            case KeyCommandType.QUICK_SELECT_CITYSHIP:
                new QuickSelectCommand(QuickSelectType.CITYSHIP).Execute(player);
                break;
            case KeyCommandType.QUICK_SELECT_BETWEEN_ALL_UNITS:
                new QuickSelectCommand(QuickSelectType.ALL_UNITS).Execute(player);
                break;
            case KeyCommandType.QUICK_SELECT_IDLE_WORKER:
                new QuickSelectCommand(QuickSelectType.IDLE_WORKER).Execute(player);
                break;
        }
    }

    void OnGUI()
    {
        if (boxSelecting)
        {
            selectionBox.Draw(player.Info.PlayerColor, Input.mousePosition, 1f);
        }
    }
}
