﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBuildingAdvancedEvent : SubjectEvent
{
    public Unit EventSender { get; set; }
    public UnitCreationPart Part { get; set; }
    public float MaxValue { get; set; }
    public float CurrentValue { get; set; }

    public UnitBuildingAdvancedEvent(Unit eventSender, UnitCreationPart part, float maxValue, float currentValue)
    {
        EventSender = eventSender;
        Part = part;
        MaxValue = maxValue;
        CurrentValue = currentValue;
    }
}
