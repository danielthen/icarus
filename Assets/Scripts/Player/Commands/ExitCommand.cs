﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitCommand : PlayerCommand
{
    public override void Execute(Player player)
    {
        player.UserInterface.EndGame();
    }
}
