﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerCursor : MonoBehaviour
{
    private Animator animator;
    private Image secondaryCommandImage;

    void Start()
    {
        animator = GetComponent<Animator>();
        secondaryCommandImage = GetComponentsInChildren<Image>()[1];
    }

    /// <summary>
    /// Change secondary command indicator image.
    /// </summary>
    /// <param name="secondaryCommandSprite"></param>
    public void ChangeSecondaryCommandIndicator(Sprite secondaryCommandSprite)
    {
        secondaryCommandImage.sprite = secondaryCommandSprite;
    }

    /// <summary>
    /// Show secondary command indicator.
    /// </summary>
    /// <param name="show">Show secondary command indicator or not.</param>
    public void ShowSecondaryCommandIndicator(bool show)
    {
        animator.SetBool("secondaryCommand", show);
    }
}
