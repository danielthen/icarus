﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulationChangeEvent : SubjectEvent
{
    public Player EventSender { get; private set; }
    public int PopulationChangeAmount { get; private set; }
    public int PopulationCapacityChangeAmount { get; private set; }

    public PopulationChangeEvent(Player eventSender, int populationChangeAmount, int populationCapacityChangeAmount)
    {
        EventSender = eventSender;
        PopulationChangeAmount = populationChangeAmount;
        PopulationCapacityChangeAmount = populationCapacityChangeAmount;
    }
}
