﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// One world grid node.
/// </summary>
public class WorldNode
{
	// Indexes from grid.
	public int gridRowIndex {get; set;}
	public int gridColumnIndex {get; set;}

	// Where is this node located in the world.
	public Vector3 WorldPosition {get; set;}

	public WorldNode(int x, int y, Vector3 worldPosition)
	{
		this.gridRowIndex = x;
		this.gridColumnIndex = y;
		this.WorldPosition = worldPosition;
	}
}
