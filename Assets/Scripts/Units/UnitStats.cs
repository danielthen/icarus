﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Unit stats: health, armour and speed.
/// </summary>
public class UnitStats : MonoBehaviour
{
    [SerializeField] private float health;
    [SerializeField] private float armour;
    [SerializeField] private float actionRange;
    [SerializeField] private float creationTime;

    // The reason why I defined health, armour, speed, attack variables was because I wanted to change them in Unity editor. It is not possible to change property values in Unity editor.
    public float Armour { get { return armour; } set { armour = value; } }
    public float Health { get { return health; } set { health = value; } }
    public float ActionRange { get { return actionRange; } set { actionRange = value; } }
    public float CreationTime { get { return creationTime; } set { creationTime = value; } }
    public float MaxHealth { get; set; }

    void Start()
    {
        MaxHealth = health;
    }

	/// <summary>
	/// Damage the unit - decrease health value.
	/// </summary>
	/// <param name="damageValue">Value.</param>
	public void Damage(float damageValue)
	{
        if (health <= 0) return;
		// Decrease unit's health value while taking account also unit's armour value.
		health -= (armour >= damageValue) ? (damageValue / armour) * damageValue : damageValue;

		if (health <= 0) 
		{
			DestroyMe ();
		}

        Unit thisUnit = GetComponent<Unit>();
        Player thisUnitPlayer = thisUnit.GetPlayer();
        thisUnitPlayer.NotifyObservers(new UnderAttackEvent(thisUnitPlayer.UserInterface));
        thisUnitPlayer.NotifyObservers(new UnitSelectionEvent(thisUnitPlayer, new List<Unit>(), new List<Unit>()));
    }

	/// <summary>
	/// Repair this unit - increase health value.
	/// </summary>
	/// <param name="repairValue">Repair value.</param>
	public void Repair(float repairValue)
	{
		health += repairValue;
	}

	/// <summary>
	/// Destroy this unit.
	/// </summary>
	public void DestroyMe()
	{
        Unit thisUnit = GetComponent<Unit>();

        Player thisUnitPlayer = thisUnit.GetPlayer();

        UnitType thisUnitType = thisUnit.Info.Type;

        if (thisUnit.Info.Type == UnitType.CITYSHIP && SceneManager.GetActiveScene().buildIndex != 1)
        {
            Game.game.EndGame(false);
        }
        else if (thisUnit.Info.Type == UnitType.WORKER && SceneManager.GetActiveScene().buildIndex == 1)
        {
            Game.game.EndGame(false);
        }

        if (thisUnitType == UnitType.DOCK   ||
            thisUnitType == UnitType.ENGINE ||
            thisUnitType == UnitType.TOWER  ||
            thisUnitType == UnitType.WEAPON)
        {
            thisUnitPlayer.Units.RemoveCityshipUnit(thisUnit);
            thisUnitPlayer.Units.GetUnitByType(UnitType.CITYSHIP).NotifyObservers(new CityshipUnitDestroyed(thisUnitPlayer.Units.GetUnitByType(UnitType.CITYSHIP), thisUnit, thisUnit.GetComponentInParent<BuildablePart>()));
        }
        else
        {
            thisUnitPlayer.Units.RemoveUnit(thisUnit);
            thisUnitPlayer.NotifyObservers(new PopulationChangeEvent(thisUnitPlayer, -1, 0));
        }
		Destroy (gameObject);
	}
}
