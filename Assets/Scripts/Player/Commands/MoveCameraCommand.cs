﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCameraCommand : PlayerCommand
{
    private Vector3 position;

    public MoveCameraCommand(Vector3 position)
    {
        this.position = position;
    }

    public override void Execute(Player player)
    {
        player.CameraController.SetPosition(position);
    }
}
