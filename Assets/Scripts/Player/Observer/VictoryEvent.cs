﻿public class VictoryEvent : SubjectEvent
{
    public Player EventSender { get; set; }

    public VictoryEvent(Player eventSender)
    {
        EventSender = eventSender;
    }
}
