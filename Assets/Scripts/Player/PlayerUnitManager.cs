﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player's unit manager. Holds all player units, selected units and also cityship units. 
/// </summary>
public class PlayerUnitManager : MonoBehaviour
{
    private Player player;

    public List<Unit> Units {get; private set;}

    public List<Unit> CityshipUnits { get; private set; }

    // All selected units.
    public List<Unit> SelectedUnits { get; private set; }

	void Start()
	{
        player = GetComponent<Player>();
		Units = new List<Unit> ();
		SelectedUnits = new List<Unit> ();
        CityshipUnits = new List<Unit> ();

		// Also add all units that were added from editor, change their secondary color and notify observer.
		foreach (Unit u in gameObject.GetComponentsInChildren<Unit> ()) 
		{
			Units.Add (u);
            ChangeColors(u.gameObject);
            player.NotifyObservers(new PopulationChangeEvent(player, 1, 0));
        }
	}

    void FixedUpdate()
	{
        if (Game.game.State == Game.GameState.PAUSED) return;
        UnitsUpdate(Units);
        UnitsUpdate(CityshipUnits);
	}

    /// <summary>
    /// Update units in specific unit list.
    /// </summary>
    /// <param name="units">Units to update.</param>
    private void UnitsUpdate(List<Unit> units)
    {
        foreach (Unit u in units.ToArray())
        {
            if (u == null) return;
            // Make unit healtbars look towards camera.
            if (u.GraphicalUserInterface != null) u.GraphicalUserInterface.transform.LookAt(Game.game.HumanPlayer.CameraController.MyCamera.transform.position);

            // Update unit specific components.
            if (u.Components == null) return;
            foreach (SpecificUnitComponent specUnComp in u.Components)
            {
                if (!specUnComp.NeedsUpdate) continue;
                specUnComp.UpdateUnit();
            }
        }
    }

	/// <summary>
	/// Creates a unit.
	/// </summary>
	/// <param name="unitPrefab">Unit prefab.</param>
    /// <param name="unitCreationPart">Part where to create unit.</param>
	public Unit CreateUnit(GameObject unitPrefab, UnitCreationPart unitCreationPart)
	{
        Unit newUnitUnitComponent = null;
        GameObject newUnit;
        switch (unitCreationPart.Type)
        {
            case UnitType.DOCK:
            case UnitType.ENGINE:
            case UnitType.TOWER:
            case UnitType.WEAPON:
                newUnit = Instantiate(unitPrefab, unitCreationPart.BuildingLocation.transform.position, unitCreationPart.BuildingLocation.transform.rotation);
                newUnit.transform.parent = unitCreationPart.transform;
                newUnitUnitComponent = newUnit.GetComponent<Unit>();
                CityshipUnits.Add(newUnitUnitComponent);
                break;
            case UnitType.BATTLESHIP:
            case UnitType.FIGHTER:
            case UnitType.WORKER:
                newUnit = Instantiate(unitPrefab, unitCreationPart.BuildingLocation.transform.position + new Vector3(Random.Range(-10f, 10f), 0, Random.Range(-10f, 0f)), unitCreationPart.BuildingLocation.transform.rotation);
                newUnit.transform.parent = player.transform;
                newUnitUnitComponent = newUnit.GetComponent<Unit>();
                if (newUnit.GetComponentInChildren<MeshRenderer>().materials[1] != null) ChangeColors(newUnit);
                Units.Add(newUnitUnitComponent);
                player.NotifyObservers(new PopulationChangeEvent(player, 1, 0));
                break;
        }

        return newUnitUnitComponent;
    }

    /// <summary>
	/// Creates a unit.
	/// </summary>
	/// <param name="type">Unit type.</param>
    /// <param name="location">Location where to create a unit.</param>
	public Unit CreateUnit(UnitType type, Transform location)
    {
        Unit newUnitUnitComponent = null;
        GameObject newUnit = null;
        GameObject unitPrefab = null;

        switch (type)
        {
            case UnitType.FIGHTER:
                unitPrefab = Resources.Load<GameObject>("Fighter");
                break;
            case UnitType.BATTLESHIP:
                unitPrefab = Resources.Load<GameObject>("BattleShip");
                break;
            default:
                break;
        }

        newUnit = Instantiate(unitPrefab, location.position, Quaternion.identity);
        newUnit.transform.parent = player.transform;
        newUnitUnitComponent = newUnit.GetComponent<Unit>();
        if (newUnit.GetComponentInChildren<MeshRenderer>().materials[1] != null) ChangeColors(newUnit);
        Units.Add(newUnitUnitComponent);

        return newUnitUnitComponent;
    }

    /// <summary>
    /// Change unit colors accordingly to player color.
    /// </summary>
    /// <param name="u"></param>
    private void ChangeColors(GameObject u)
    {
        if (u.GetComponentInChildren<MeshRenderer>().materials.Length < 2) return;
        if (u.GetComponentInChildren<MeshRenderer>().materials[1] != null)
        {
            Color col = GetComponent<Player>().Info.PlayerColor;
            if (u.GetComponent<UnitInfo>().Type == UnitType.CITYSHIP)
            {
                u.GetComponentInChildren<MeshRenderer>().materials[0].color = col;
            }
            else
            {
                u.GetComponentInChildren<MeshRenderer>().materials[1].color = col;
            }
            u.GetComponentInChildren<SpriteRenderer>().color = col;

            TrailRenderer tr = u.GetComponent<TrailRenderer>();
            if (tr == null) return;
            tr.materials[0].color = col;
        }
    }

	/// <summary>
	/// Removes a unit from this class.
	/// </summary>
	/// <param name="unit">Unit.</param>
	public void RemoveUnit(Unit unit)
	{
		Units.Remove (unit);
		DeselectSpecificUnit (unit);
    }

    /// <summary>
	/// Removes a city ship building unit from this class.
	/// </summary>
	/// <param name="unit">Unit.</param>
	public void RemoveCityshipUnit(Unit unit)
    {
        CityshipUnits.Remove(unit);
        DeselectSpecificUnit(unit);
    }

    /// <summary>
    /// Select given units.
    /// </summary>
    /// <param name="units"></param>
	public void SelectUnits(List<Unit> units)
	{
        foreach(Unit unit in units)
        {
            SelectedUnits.Add(unit);
        }
	}

    /// <summary>
    ///  Deselects all currently selected units.
    /// </summary>
    /// <returns>Units that were previously selected.</returns>
    public List<Unit> DeselectUnits()
	{
        List<Unit> selectedUnitsCopy = new List<Unit>();

        foreach (Unit unit in SelectedUnits)
        {
            if(unit != null)
            {
                selectedUnitsCopy.Add(unit);
            }
        }

		SelectedUnits.Clear ();

        return selectedUnitsCopy;
	}

	/// <summary>
	/// Deselects a specific unit.
	/// </summary>
	/// <param name="unit">Unit to deselect.</param>
    /// <returns>Unit that was deselected. </returns>
	public Unit DeselectSpecificUnit(Unit unit)
	{
		if (unit == null) return null;

		if (!SelectedUnits.Contains (unit)) return null;

        Unit copy = unit;

        SelectedUnits.Remove (unit);

        return copy;
	}
    
    /// <summary>
    /// Is specific unit selected.
    /// </summary>
    /// <param name="unit"></param>
    /// <returns>Is specific unit selected.</returns>
    public bool IsSelected(Unit unit)
    {
        return SelectedUnits.Contains(unit);
    }

	/// <summary>
	/// Adds a command to selected units.
	/// </summary>
	/// <param name="command">Command to add.</param>
	/// </param name="addCommandToQueue">Choose if add command to queue or execute it immediately.</param> 
	public void AddCommandToSelectedUnits(UnitCommand command, bool addCommandToQueue)
	{
		foreach (Unit u in SelectedUnits)
		{
			if (addCommandToQueue) 
			{
				u.Commander.AddCommandToQueue (command);
			}
			else
			{
				u.Commander.AddCommand (command);
			}
		}
	}


    /// <summary>
	/// Adds a command to all units with specific type.
	/// </summary>
	/// <param name="command">Command to add.</param>
	/// </param name="addCommandToQueue">Choose if add command to queue or execute it immediately.</param>
    /// <param name="type>Unit's type.</param>
	public void AddCommandToSpecificUnitType(UnitCommand command, bool addCommandToQueue, UnitType type)
    {
        foreach (Unit u in Units)
        {
            if (u.Info.Type != type) continue;

            if (addCommandToQueue)
            {
                u.Commander.AddCommandToQueue(command);
            }
            else
            {
                u.Commander.AddCommand(command);
            }
        }
    }

    public bool HasMoreThanOneUnitSelected { get { return SelectedUnits.Count > 1; } }

    public int SelectedUnitsCount { get { return SelectedUnits.Count; } }

    /// <summary>
    /// Get a selected unit by index.
    /// </summary>
    /// <param name="index"></param>
    /// <returns>Unit by index, otherwise null if there isn't unit with the index.</returns>
    public Unit GetSelectedUnitByIndex(int index)
    {
        if (index >= SelectedUnits.Count) return null;

        return SelectedUnits[index];
    }

    /// <summary>
    /// Get unit by type if it exist.
    /// </summary>
    /// <param name="type"></param>
    /// <returns>First unit with specified type - if unit doesn't exist then null.</returns>
    public Unit GetUnitByType(UnitType type)
    {
        foreach (Unit un in Units)
        {
            if (un.Info.Type == type)
            {
                return un;
            }
        }

        return null;
    }

    /// <summary>
    /// Get all units by type.
    /// </summary>
    /// <param name="type"></param>
    /// <returns>List of all units with specific type.</returns>
    public List<Unit> GetAllUnitsByType(UnitType type)
    {
        List<Unit> specificUnits = new List<Unit>();

        for (int i = 0; i < Units.Count; i++)
        {
            if (Units[i].Info.Type == type)
            {
                specificUnits.Add(Units[i]);
            }
        }

        return specificUnits;
    }

    /// <summary>
    /// Is unit with specific type selected.
    /// </summary>
    /// <returns>True if is - otherwise false.</returns>
    /// <param name="type">The specific type.</param>
    public bool HasUnitWithSpecificTypeSelected(UnitType type)
    {
        foreach (Unit u in SelectedUnits)
        {
            if (u.Info.Type == type)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Is there a unit that is selected and has some specific component that haves secondary command.
    /// </summary>
    /// <returns>True if so - false otherwise.</returns>
    public bool HasUnitWithSecondayCommand()
    {
        foreach(Unit u in SelectedUnits)
        {
            SpecificUnitComponent[] specUnComps = u.GetComponents<SpecificUnitComponent>();

            if(specUnComps.Length == 1)
            {
                if (specUnComps[0] is UnitBuildingComponent) return false;
            }

            if (specUnComps.Length > 0)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Clear all commands for selected units.
    /// </summary>
    public void ClearAllSelectedUnitCommands()
    {
        foreach (Unit u in SelectedUnits)
        {
            u.Commander.ClearCommands();
        }
    }

    public bool CityShipHasWeapons
    {
        get
        {
            foreach (Unit u in CityshipUnits)
            {
                if (u.Info.Type == UnitType.WEAPON)
                {
                    return true;
                }
            }

            return false;
        }
    }
}