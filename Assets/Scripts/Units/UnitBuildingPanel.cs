﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitBuildingPanel : MonoBehaviour
{
    private Image buildingImage;
    private Animator animator;
    private Sprite[] buildingImages;

    void Start()
    {
        animator = GetComponent<Animator>();
        buildingImages = Resources.LoadAll<Sprite>("icons_buildings");
        buildingImage = GetComponent<Image>();
    }

    /// <summary>
    /// Show or hide building panel.
    /// </summary>
    /// <param name="value"></param>
    public void Show(bool value)
    {
        animator.SetBool("show", value);
    }
    
    /// <summary>
    /// Change building image according to building's type.
    /// </summary>
    /// <param name="type"></param>
    public void ChangeBuildingImage(UnitType type)
    {
        switch (type)
        {
            case UnitType.DOCK:
                buildingImage.sprite = buildingImages[0];
                break;
            case UnitType.ENGINE:
                buildingImage.sprite = buildingImages[1];
                break;
            case UnitType.TOWER:
                buildingImage.sprite = buildingImages[3];
                break;
            case UnitType.WEAPON:
                buildingImage.sprite = buildingImages[2];
                break;
        }
    }

    /// <summary>
    /// Show progress how much time is stil needed to create a unit.
    /// </summary>
    /// <param name="currentValue"></param>
    /// <param name="maxValue"></param>
    public void ShowProgress(float currentValue, float maxValue)
    {
        buildingImage.fillAmount = 1 - (currentValue / maxValue);
    }

    /// <summary>
    /// Reset progress icon.
    /// </summary>
    public void ResetProgress()
    {
        buildingImage.fillAmount = 1f;
    }
}
