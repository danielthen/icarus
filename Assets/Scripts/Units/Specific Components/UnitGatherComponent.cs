﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Component for gathering.
/// </summary>
public class UnitGatherComponent : SpecificUnitComponent
{
    public Resource ResourceToCollectFrom { get; set; }
    public Player ThisUnitPlayer { get; set; }
    private LineRenderer lineRenderer;
    private float currentActionTime;
    private float gatherCooldown;
    private int gatherRate;

    void Start()
    {
        InitializeValues();
    }

    public override void UpdateUnit()
    {
        if (thisUnit.State != UnitState.GATHERING) { EndGatheringInterrupted(); return; }
        if (ResourceToCollectFrom == null) { EndGathering(); return; }

        currentActionTime += Time.deltaTime;
        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, transform.position + new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f)));
        lineRenderer.SetPosition(2, ResourceToCollectFrom.transform.position + new Vector3(Random.Range(0, 10f), Random.Range(0, 10f), Random.Range(0, 10f)));

        if (currentActionTime < gatherCooldown) return;

        // Collect resource.
        int collectAmount = ResourceToCollectFrom.Take(gatherRate);
        ThisUnitPlayer.NotifyObservers(new ResourceChangeEvent(ThisUnitPlayer, collectAmount, ResourceToCollectFrom.Type));

        // Check if resource is depleted and try to find next same type resource to collect that is near to unit (100 units right now).
        if (ResourceToCollectFrom.Value <= 0)
        {
            Resource oldResource = ResourceToCollectFrom;
            Destroy(ResourceToCollectFrom.gameObject);
            ResourceToCollectFrom = null;
            FindNextSameTypeResourceToCollect(100f, oldResource);
        }

        currentActionTime = 0f;
    }

    /// <summary>
    /// If resource is depleted try to find another to collect if there aren't any other commands this unit has to execute.
    /// </summary>
    /// <param name="distance"></param>
    /// <param name="oldResource"></param>
    private void FindNextSameTypeResourceToCollect(float distance, Resource oldResource)
    {
        UnitCommander uc = thisUnit.Commander;
        if (uc.HasMoreCommandsToExecute) { EndGathering(); return; }

        Resource[] allResources = FindObjectsOfType<Resource>();

        // Try to find new resource.
        foreach (Resource resource in allResources)
        {
            if(resource.Type == oldResource.Type && oldResource != resource && Vector3.Distance(resource.transform.position, thisUnit.transform.position) <= distance)
            {
                thisUnit.Commander.AddCommand(new MoveCommand(resource.transform, MoveCommandType.GATHER, new List<Unit> { thisUnit }));
                thisUnit.Commander.AddCommandToQueue(new GatherCommand(resource, ThisUnitPlayer));
                return;
            }
        }

        // If new resource isn't found.
        EndGathering();
    }

    public void StartGathering()
    {
        if (Vector3.Distance(transform.position, ResourceToCollectFrom.transform.position) > thisUnit.Stats.ActionRange) { EndGathering(); return; }
        lineRenderer.positionCount = 3;
        currentActionTime = 0f;
        NeedsUpdate = true;
    }

    private void EndGathering()
    {
        lineRenderer.positionCount = 0;
        if (thisUnit.State == UnitState.GATHERING)
        {
            thisUnit.State = UnitState.IDLE;
        }
        NeedsUpdate = false;
        thisUnit.NotifyObservers(new CommandExecutedEvent(thisUnit.Commander));
    }

    private void EndGatheringInterrupted()
    {
        lineRenderer.positionCount = 0;
        NeedsUpdate = false;
    }

    public void InitializeValues()
    {
        NeedsUpdate = false;
        thisUnit = GetComponent<Unit>();
        ThisUnitPlayer = thisUnit.GetPlayer();
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        gatherCooldown = 2f;
        gatherRate = 2;
    }
}