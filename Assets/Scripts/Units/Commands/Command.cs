﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all commands.
/// </summary>
public abstract class Command
{
	/// <summary>
	/// Execute a command on receiver.
	/// </summary>
	/// <param name="receiver">Receiver.</param>
	public abstract void Execute (MonoBehaviour reciever);
}
