﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseOverEvent : SubjectEvent
{
    public Player EventSender { get; private set; }
    public MonoBehaviour MouseOverObject { get; private set; }
    public string HelperText { get; private set; }

    public MouseOverEvent(Player eventSender, MonoBehaviour mouseOverObject, string helperText)
    {
        EventSender = eventSender;
        MouseOverObject = mouseOverObject;
        HelperText = helperText;
    }
}