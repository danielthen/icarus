﻿using UnityEngine;

public class AIPlayerController : PlayerControl
{
    private enum AIPlayerState { SPAWNING, PATROLLING, ATTACKING, FINAL_BATTLE}
    private PlayerUnitManager units;
    public int MaxUnitCount { get; set; }
    private AIPlayerState state;
    private float currentTime;
    public float Cooldown;

    void Start()
    {
        units = GetComponent<Player>().Units;
        state = AIPlayerState.SPAWNING;
    }

    void Update()
    {
        if (Game.game.State == Game.GameState.PAUSED || Game.game.State == Game.GameState.OVER) return;

        currentTime += Time.deltaTime;

        if(currentTime > Cooldown)
        {
            currentTime = 0f;
            switch (state)
            {
                case AIPlayerState.SPAWNING:
                    SpawnUnits();
                    state = AIPlayerState.PATROLLING;
                    FinalBattleCheck();
                    break;
                case AIPlayerState.PATROLLING:
                    state = AIPlayerState.ATTACKING;
                    Player humanPlayer = Game.game.HumanPlayer;
                    humanPlayer.NotifyObservers(new EnemyAttackEvent(humanPlayer));
                    FinalBattleCheck();
                    break;
                case AIPlayerState.ATTACKING:
                    state = AIPlayerState.SPAWNING;
                    if (MaxUnitCount + 1 < 80) MaxUnitCount++;
                    if (Cooldown + 10f < 120f) Cooldown += 10f;
                    FinalBattleCheck();
                    break;
                case AIPlayerState.FINAL_BATTLE:
                    SpawnUnits();
                    break;
            }
        }

        switch (state)
        {
            case AIPlayerState.SPAWNING:
                SpawnUnits();
                break;
            case AIPlayerState.PATROLLING:
                AddCommandToAllUnits(new MoveCommand(new Vector3(transform.position.x + Random.Range(0f, 200f), 0f, transform.position.z + Random.Range(0f, 200f)), MoveCommandType.DEFAULT, units.Units));
                break;
            case AIPlayerState.ATTACKING:
                Attack();
                break;
            case AIPlayerState.FINAL_BATTLE:
                Attack();
                break;
        }
    }

    /// <summary>
    /// Spawn units.
    /// </summary>
    private void SpawnUnits()
    {
        if (units.Units.Count > MaxUnitCount) return;

        int[] randomUnitType = { 1, 2 };

        for (int i = 0; i < MaxUnitCount; i++)
        {
            if (units.Units.Count + 1 >= MaxUnitCount) return; 
            units.CreateUnit((UnitType) randomUnitType[Random.Range(0, randomUnitType.Length)], transform);
        }
    }

    /// <summary>
    /// Add command to all units.
    /// </summary>
    /// <param name="command"></param>
    private void AddCommandToAllUnits(UnitCommand command)
    {
        for (int i = 0; i < units.Units.Count; i++)
        {
            if (units.Units[i] == null) continue;
            if (units.Units[i].State != UnitState.IDLE) continue;
            units.Units[i].Commander.AddCommand(command);
        }
    }

    /// <summary>
    /// Attack random human player cityship.
    /// </summary>
    private void Attack()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        PlayerUnitManager humanUnits = humanPlayer.Units;
        Unit unitToAttack = null;

        for (int i = 0; i < humanUnits.Units.Count; i++)
        {
            Unit target = humanUnits.Units[i];
            if (target == null) continue;
            if (target.Info.Type == UnitType.CITYSHIP)
            {
                unitToAttack = target;
                break;
            }
        }

        AddCommandToAllUnits(new AttackCommand(unitToAttack));
    }

    private void FinalBattleCheck()
    {
        if(Vector3.Distance(Game.game.HumanPlayer.Units.GetUnitByType(UnitType.CITYSHIP).transform.position, transform.position) < 100f)
        {
            state = AIPlayerState.FINAL_BATTLE;
        }
    }
}
