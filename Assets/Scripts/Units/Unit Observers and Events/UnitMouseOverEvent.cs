﻿public class UnitMouseOverEvent : SubjectEvent
{
    public Unit ThisUnit { get; set; }

    public UnitMouseOverEvent(Unit thisUnit)
    {
        ThisUnit = thisUnit;
    }
}