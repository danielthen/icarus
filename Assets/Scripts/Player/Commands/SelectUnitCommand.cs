﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Select a single unit.
/// </summary>
public class SelectUnitCommand : PlayerCommand 
{
	// Gameobject to select.
	private List<GameObject> selected;
    // Does player want to keep previously selected units selected.
    private bool dontClearUnitsThatWereSelectedBefore;

	public SelectUnitCommand(List<GameObject> selected, bool dontClearUnitsThatWereSelectedBefore)
	{
		this.selected = selected;
        this.dontClearUnitsThatWereSelectedBefore = dontClearUnitsThatWereSelectedBefore;
	}

    public SelectUnitCommand(GameObject selected, bool dontClearUnitsThatWereSelectedBefore)
	{
        this.selected = new List<GameObject>
        {
            selected
        };
        this.dontClearUnitsThatWereSelectedBefore = dontClearUnitsThatWereSelectedBefore;
	}

	public override void Execute (Player player)
	{
        bool selectAlsoAllSameTypeUnits = false;

        List<Unit> unitsToSelect = new List<Unit>();
        List<Unit> unitsThatWereSelected = new List<Unit>();

        // If there isn't anything to select then deselect everything.
        if (selected.Count <= 0) { player.Units.DeselectUnits(); return; }

        // Double click selects all same type units.
        if (selected.Count == 1 && player.Units.SelectedUnitsCount == 1 && !dontClearUnitsThatWereSelectedBefore)
        {
            selectAlsoAllSameTypeUnits = true;
        }

        foreach (GameObject select in selected)
        {
            Unit unit = select.GetComponent<Unit>();

            if(unit != null)
            {
                // Trying to get unit's parent player.
                Player parentPlayer = unit.GetPlayer();

                // Verifying if unit has player parent and it is the same player trying to select target.
                if (parentPlayer == null) continue;
                if (parentPlayer != player) continue;

                if(player.Units.IsSelected(unit) && dontClearUnitsThatWereSelectedBefore)
                {
                    unitsThatWereSelected.Add(player.Units.DeselectSpecificUnit(unit));
                }
                else
                {
                    // Prepare unit for selection if it passes all requirements.
                    unitsToSelect.Add(unit);
                }
                
            }
        }

        // Deselect currently selected units.
        if (!dontClearUnitsThatWereSelectedBefore)
        {
            unitsThatWereSelected = player.Units.DeselectUnits();
        }

        // Select all the same units.
        if(selectAlsoAllSameTypeUnits)
        {
            if(unitsToSelect.Count > 0 && unitsThatWereSelected[0] == unitsToSelect[0])
            {
                foreach (Unit u in player.Units.Units)
                {
                    Renderer renderer = null;

                    foreach (Renderer r in u.GetComponentsInChildren<Renderer>())
                    {
                        if(r.gameObject.tag == "Model")
                        {
                            renderer = r;
                        }
                    }

                    if (u.Info.Type == unitsToSelect[0].Info.Type &&  u != unitsToSelect[0] && renderer != null)
                    {
                        if(renderer.isVisible)
                        {
                            unitsToSelect.Add(u);
                        }
                    }
                }
            }
        }

        // Finally select units.
        player.Units.SelectUnits(unitsToSelect);

        // Notify observers.
        player.NotifyObservers(new UnitSelectionEvent(player, unitsToSelect, unitsThatWereSelected));
	}
}