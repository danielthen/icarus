﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Priority queue for A* algorithm.
/// </summary>
public class PriorityQueue<T>
{
	/*
	 * Binary Heap
	 * Implementation based on Jüri Kiho's "Algoritmid ja andmestruktuurid" book of year 2003.
	 * Pages 52 - 58
	 */

	private List<PriorityNode<T>> binaryHeap;

	public PriorityQueue()
	{
		binaryHeap = new List<PriorityNode<T>>();
	}

	/// <summary>
	/// Gets left child index of a node.
	/// </summary>
	/// <returns>Lef child index.</returns>
	/// <param name="index">Index.</param>
	private int LeftChild(int index)
	{
		return 2 * index + 1;
	}	

	/// <summary>
	/// Gets right child index of a node.
	/// </summary>
	/// <returns>Right child index.</returns>
	/// <param name="index">Index.</param>
	private int RightChild(int index)
	{
		return 2 * index + 2;
	}	

	/// <summary>
	/// Gets parent index of a node.
	/// </summary>
	/// <returns>Parent index.</returns>
	/// <param name="index">Index.</param>
	private int Parent(int index)
	{
		return (int) Mathf.Floor((index - 1) / 2);
	}	

	/// <summary>
	/// Brings up a node in binary heap.
	/// </summary>
	/// <param name="index">Index of the node.</param>
	private void BringUp(int index)
	{
		int parentIndex = Parent (index);

		// If node is not root.
		if (parentIndex >= 0 && parentIndex < Count) 
		{
			if (binaryHeap [index].Value > binaryHeap [parentIndex].Value) 
			{
				PriorityNode<T> holder = binaryHeap [index];
				binaryHeap [index] = binaryHeap [parentIndex];
				binaryHeap [parentIndex] = holder;
				BringUp (parentIndex);
			}
		}
	}

	/// <summary>
	/// Brings down a node in binary heap.
	/// </summary>
	/// <param name="index">Index of the node.</param>
	private void BringDown(int index)
	{
		int leftChildIndex = LeftChild (index);

		if (leftChildIndex >= 0 && leftChildIndex < Count)
		{
			int rightChildIndex = RightChild (index);

			if (rightChildIndex >= 0 && rightChildIndex < Count) 
			{
				if (binaryHeap [rightChildIndex].Value > binaryHeap [leftChildIndex].Value) 
				{
					leftChildIndex = rightChildIndex;
				}
			}

			if (binaryHeap[leftChildIndex].Value > binaryHeap[index].Value)
			{
				PriorityNode<T> nodeHolder = binaryHeap [leftChildIndex];
				binaryHeap [leftChildIndex] = binaryHeap [index];
				binaryHeap [index] = nodeHolder;
				BringDown (leftChildIndex);
			}
		}
	}

	/// <summary>
	/// Add the specified element with value to queue.
	/// </summary>
	/// <param name="element">Element.</param>
	/// <param name="value">Value.</param>
	public void Add(T element, float value)
	{
		binaryHeap.Add (new PriorityNode<T>(element, value));
		BringUp (Count - 1);
	}

	/// <summary>
	/// Get first element's content.
	/// </summary>
	public T Peek()
	{
		if (binaryHeap.Count > 0) 
		{
			return binaryHeap [0].Content;
		}
		
		return default(T);
	}

	/// <summary>
	/// Take first element from binary heap.
	/// </summary>
	public T Take()
	{
		if (Count <= 0) return default(T);

		T contentToReturn = binaryHeap [0].Content;
		binaryHeap [0] = binaryHeap [Count - 1];
		binaryHeap.RemoveAt (Count - 1);

		BringDown (0);

		return contentToReturn;
	}

	/// <summary>
	/// Clears the queue.
	/// </summary>
	public void Clear()
	{
		binaryHeap.Clear ();
	}

	public int Count {get { return binaryHeap.Count;} }

	public override string ToString ()
	{
		string toString = "";

		foreach(PriorityNode<T> node in binaryHeap)
		{
			toString += node.Content;
		}

		return toString;
	}
}