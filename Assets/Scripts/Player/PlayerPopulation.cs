﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPopulation
{
    public int CurrentPopulation {get; set;}
    public int MaxPopulation {get; set;}
}
