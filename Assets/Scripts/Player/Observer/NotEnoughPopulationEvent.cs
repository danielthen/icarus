﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotEnoughPopulationEvent : SubjectEvent
{
    public PlayerUI UserInterface { get; set; }

    public NotEnoughPopulationEvent(PlayerUI userInterface)
    {
        UserInterface = userInterface;
    }
}
