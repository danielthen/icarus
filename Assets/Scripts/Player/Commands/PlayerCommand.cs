﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all player commands.
/// </summary>
public abstract class PlayerCommand
{
	/// <summary>
	/// Execute a command on receiver.
	/// </summary>
	/// <param name="receiver">Receiver.</param>
	public abstract void Execute (Player player);
}
