﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] private List<GameObject> planets;
    [SerializeField] private float rotationSpeed;

    void Update()
    {
        if (planets == null) return;

        foreach(GameObject planet in planets)
        {
            planet.transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
        }
    }
}
