﻿using System;

public class UnitGUIObserver : Observer
{
    public override void OnNotify(SubjectEvent subEvent)
    {
        if (subEvent is BuildablePartMouseOverEvent)
        {
            BuildablePartMouseOverEvent bpmoe = subEvent as BuildablePartMouseOverEvent;
            if (bpmoe.EventSender.Cityship.State == UnitState.BUILDING) return;
            bpmoe.BuildingPanel.ChangeBuildingImage(bpmoe.EventSender.Type);
            bpmoe.BuildingPanel.Show(true);
        }
        else if (subEvent is BuildablePartOnMouseExitEvent)
        {
            BuildablePartOnMouseExitEvent bpomee = subEvent as BuildablePartOnMouseExitEvent;
            if (bpomee.EventSender.Cityship.State == UnitState.BUILDING) return;
            bpomee.BuildingPanel.Show(false);
        }
        else if (subEvent is UnitMouseOverEvent)
        {
            UnitMouseOverEvent umoe = subEvent as UnitMouseOverEvent;
            umoe.ThisUnit.GraphicalUserInterface.UnitHealthBar.Show(true);
        }
        else if (subEvent is UnitMouseExitEvent)
        {
            UnitMouseExitEvent umee = subEvent as UnitMouseExitEvent;

            Player thisPlayer = umee.ThisUnit.GetPlayer();

            if (thisPlayer == null) { umee.ThisUnit.GraphicalUserInterface.UnitHealthBar.Show(false); return; }
            if (thisPlayer != Game.game.HumanPlayer) { umee.ThisUnit.GraphicalUserInterface.UnitHealthBar.Show(false); return; }
            // If unit is selected then we don't want to hide its GUI.
            if (thisPlayer.Units.IsSelected(umee.ThisUnit)) return;
            // Otherwise we want to hide its GUI.
            umee.ThisUnit.GraphicalUserInterface.UnitHealthBar.Show(false);
        }
        else if (subEvent is UnitBuildingStartEvent)
        {
            UnitBuildingStartEvent ubse = subEvent as UnitBuildingStartEvent;

            if(ubse.Part is BuildablePart)
            {
                UnitBuildingPanel ubp = ubse.EventSender.GetComponentInChildren<UnitBuildingPanel>();
                ubp.ChangeBuildingImage(ubse.Part.Type);
                ubp.Show(true);
            }
        }
        else if (subEvent is UnitBuildingFinishedEvent)
        {
            UnitBuildingFinishedEvent ubfe = subEvent as UnitBuildingFinishedEvent;

            if (ubfe.Part is BuildablePart)
            {
                UnitBuildingPanel ubp = ubfe.EventSender.GetComponentInChildren<UnitBuildingPanel>();
                ubp.Show(false);
                ubp.ResetProgress();
                (ubfe.Part as BuildablePart).HasBuilding = true;
            }
            else
            {
                CreatablePart cp = ubfe.Part as CreatablePart;
                DockGUI dockGUI = ubfe.EventSender.GetComponentInChildren<DockGUI>();
                dockGUI.ResetProgress(cp);
            }
        }
        else if (subEvent is UnitBuildingAdvancedEvent)
        {
            UnitBuildingAdvancedEvent ubae = subEvent as UnitBuildingAdvancedEvent;

            if(ubae.Part is BuildablePart)
            {
                UnitBuildingPanel ubp = ubae.EventSender.GetComponentInChildren<UnitBuildingPanel>();
                ubp.ShowProgress(ubae.CurrentValue, ubae.MaxValue);
            }
            else
            {
                CreatablePart cp = ubae.Part as CreatablePart;
                try
                {
                    DockGUI dockGUI = ubae.EventSender.GetComponentInChildren<DockGUI>();
                    dockGUI.ShowProgress(cp, ubae.CurrentValue, ubae.MaxValue);
                }
                catch (Exception) { return; }
            }
        }
        else if (subEvent is UnitBuildingInterruptedEvent)
        {
            UnitBuildingInterruptedEvent ubie = subEvent as UnitBuildingInterruptedEvent;
            if (ubie.Part is BuildablePart)
            {
                (ubie.Part as BuildablePart).HasBuilding = false;
                UnitBuildingPanel ubp = ubie.EventSender.GetComponentInChildren<UnitBuildingPanel>();
                ubp.Show(false);
                ubp.ResetProgress();
            }
            else
            {
                CreatablePart cp = ubie.Part as CreatablePart;
                DockGUI dockGUI = ubie.EventSender.GetComponentInChildren<DockGUI>();
                dockGUI.ResetProgress(cp);
            }
        }
        else if (subEvent is UnitHurtEvent)
        {
            UnitHurtEvent uhe = subEvent as UnitHurtEvent;

            UnitStats us = uhe.EventSender.Stats;

            uhe.EventSender.GraphicalUserInterface.UnitHealthBar.UpdateHealthBar(us.Health / us.MaxHealth);

            if(uhe != null && uhe.EventSender.State != UnitState.ATTACKING)
            {
                uhe.EventSender.Commander.AddCommand(new AttackCommand(uhe.Attacker));
            }
        }
    }
}
