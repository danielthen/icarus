﻿using UnityEngine;

public class NotEnoughResourcesEvent : SubjectEvent
{
    public PlayerUI UserInterface { get; set; }

    public NotEnoughResourcesEvent(PlayerUI userInterface)
    {
        UserInterface = userInterface;
    }
}