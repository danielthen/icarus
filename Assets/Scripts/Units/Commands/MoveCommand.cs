﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Give command to move a unit.
/// </summary>
public class MoveCommand : UnitCommand 
{
	private readonly Vector3 position;
    private readonly MoveCommandType moveType;
    private readonly Transform transform;
    private readonly List<Unit> selectedUnits;

	public MoveCommand(Vector3 position, MoveCommandType moveType, List<Unit> selectedUnits)
	{
		this.position = position;
        this.moveType = moveType;
        this.selectedUnits = selectedUnits;
    }

    public MoveCommand(Transform transform, MoveCommandType moveType, List<Unit> selectedUnits)
	{
		this.transform = transform;
        this.moveType = moveType;
        this.selectedUnits = selectedUnits;
    }

	public override void Execute (Unit reciever)
	{
        if (reciever.GetComponent<UnitMovementComponent>() == null) { reciever.NotifyObservers(new CommandExecutedEvent(reciever.Commander)); return; }
        reciever.State = UnitState.MOVING;

        UnitMovementComponent umc = reciever.GetComponent<UnitMovementComponent>();

        switch(moveType)
        {
            case MoveCommandType.DEFAULT:
                umc.StoppingDistance = 5f;
                int index = selectedUnits.IndexOf(reciever);
                Vector3 newPosition = position + new Vector3((index - (10 * Mathf.Floor(index / 10))) * 35f, 0f, Mathf.Floor((index / 10)) * 35f);
                umc.MoveToPosition(newPosition);
                break;
            case MoveCommandType.GATHER:
                umc.StoppingDistance = 5f;
                if (reciever.Info.Type != UnitType.WORKER) break;
                if (transform == null) break;
                Vector3 pos = transform.GetComponent<Resource>().GetAvailableGatherPosition();
                if (pos == Vector3.zero) break;
                umc.MoveToPosition(pos);
                return;
            case MoveCommandType.ATTACK:
                if(Vector3.Distance(reciever.transform.position, position) <= reciever.Stats.ActionRange)
                {
                    reciever.State = UnitState.IDLE;
                    reciever.NotifyObservers(new CommandExecutedEvent(reciever.Commander));
                    return;
                }
                umc.StoppingDistance = 25f;
                Vector3 randomPosOnUnitSphere = Random.onUnitSphere * 100f;
                umc.MoveToPosition(position + new Vector3(randomPosOnUnitSphere.x, 0f, randomPosOnUnitSphere.z));
                break;
        }
    }
}