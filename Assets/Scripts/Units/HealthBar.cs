﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Image healthBar;

    void Start()
    {
        healthBar = GetComponentInChildren<Image>();
        Show(false);
    }

    /// <summary>
    /// Update health bar by telling it how much it should be filled.
    /// </summary>
    /// <param name="fillPercent"></param>
    public void UpdateHealthBar(float fillPercent)
    {
        healthBar.fillAmount = fillPercent;
    }

    /// <summary>
    /// Show healthbar.
    /// </summary>
    public void Show(bool value)
    {
        if(value)
        {
            healthBar.color = new Color(0f, 1f, 0f, 1f);
        }
        else
        {
            healthBar.color = new Color(0f, 0f, 0f, 0f);
        }
    }
}
