﻿using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour
{
    [SerializeField] private ResourceType _type;
    public ResourceType Type {get {return _type;}}
    public int Value {get; set;}
    [SerializeField] private GameObject model;
    private int maxValue;
    private float maxSize;

    private List<Vector3> gatherPositions;

    void Start()
    {
        Value = 1000;
        maxValue = Value;
        maxSize = model.transform.localScale.x;
        InitializeGatherPositions();
    }

    /// <summary>
    /// Takes amount from this resources value.
    /// </summary>
    /// <param name="quantity"></param>
    /// <returns>Taken amount of this resource.</returns>
    public int Take(int quantity)
    {
        if(Value - quantity <= 0)
        {
            int value = Value;
            Value = 0;
            return value;
        }

        Value -= quantity;

        float currentValuePercent = (float) Value / maxValue;
        currentValuePercent *= maxSize; 
        model.transform.localScale = new Vector3(currentValuePercent, currentValuePercent, currentValuePercent);

        return quantity;
    }

    /// <summary>
    /// Initialize gather positions for units.
    /// </summary>
    public void InitializeGatherPositions()
    {
        gatherPositions = new List<Vector3>();

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            if (child.tag == "GatherLocation")
            {
                gatherPositions.Add(child.position);
            }
        }
    }

    /// <summary>
    /// Get gather position.
    /// </summary>
    /// <returns>Random gather position.</returns>
    public Vector3 GetAvailableGatherPosition()
    {
        return gatherPositions[Random.Range(0, gatherPositions.Count)];
    }

    void OnMouseEnter()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        humanPlayer.NotifyObservers(new MouseOverEvent(humanPlayer, this, GetCorrectFeedBack() + " COLLECT it in order to build new units and buildings."));
    }

    void OnMouseExit()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        humanPlayer.NotifyObservers(new MouseOverExitEvent(humanPlayer, this));
    }

    /// <summary>
    /// Feedback for mouse over event.
    /// </summary>
    /// <returns></returns>
    private string GetCorrectFeedBack()
    {
        switch(_type)
        {
            case ResourceType.METAL:
                return "METAL RESOURCE: " + Value + " left.";
            case ResourceType.SOLAR_POWER:
                return "SOLAR POWER RESOURCE: " + Value + " left.";
        }

        return "";
    }
}