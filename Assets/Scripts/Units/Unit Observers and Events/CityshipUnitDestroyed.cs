﻿public class CityshipUnitDestroyed : SubjectEvent
{
    public Unit EventSender { get; set; }
    public Unit DestroyedUnit { get; set; }
    public BuildablePart Part { get; set; }

    public CityshipUnitDestroyed(Unit eventSender, Unit destroyedUnit, BuildablePart part)
    {
        EventSender = eventSender;
        DestroyedUnit = destroyedUnit;
        Part = part;
    }
}
