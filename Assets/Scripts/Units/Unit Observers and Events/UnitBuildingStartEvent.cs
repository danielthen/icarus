﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBuildingStartEvent : SubjectEvent
{
    public Unit EventSender { get; set; }
    public UnitCreationPart Part { get; set; }

    public UnitBuildingStartEvent(Unit eventSender, UnitCreationPart part)
    {
        EventSender = eventSender;
        Part = part;
    }
}
