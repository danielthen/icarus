﻿using UnityEngine;

/// <summary>
/// Unit component for building new units.
/// </summary>
public class UnitBuildingComponent : SpecificUnitComponent
{
    private UnitCreationPart currentPart;
    private GameObject currentBuildUnit;
    private float currentActionTime;
    private float buildRate;

    void Start()
    {
        thisUnit = GetComponent<Unit>();
        currentActionTime = 0f;
        buildRate = 2f;
    }

    public override void UpdateUnit()
    {
        if (thisUnit.State != UnitState.BUILDING ) { EndBuildingInterrputed(); return; }
        if (currentBuildUnit == null) { EndBuildingInterrputed(); return; }

        currentActionTime += buildRate * Time.deltaTime;
        thisUnit.NotifyObservers(new UnitBuildingAdvancedEvent(thisUnit, currentPart, currentBuildUnit.GetComponent<UnitStats>().CreationTime, currentActionTime));

        if (currentActionTime >= currentBuildUnit.GetComponent<UnitStats>().CreationTime)
        {
            Unit createdUnit = thisUnit.GetPlayer().Units.CreateUnit(currentBuildUnit, currentPart);
            EndBuildingNormal(createdUnit);
        }
    }

    /// <summary>
    /// Start build a building on a given buildable part.
    /// </summary>
    /// <param name="unitCreationPart">Buildable part.</param>
    public void StartBuilding(UnitCreationPart unitCreationPart)
    {
        if (unitCreationPart == null) return;

        currentActionTime = 0f;

        currentBuildUnit = GetUnitPrefab(unitCreationPart.Type);

        thisUnit.NotifyObservers(new UnitBuildingStartEvent(thisUnit, unitCreationPart));

        currentPart = unitCreationPart;

        NeedsUpdate = true;

        Player thisUnitPlayer = thisUnit.GetPlayer();
        thisUnitPlayer.NotifyObservers(new ResourceChangeEvent(thisUnitPlayer, (int)-currentPart.Cost.x, ResourceType.SOLAR_POWER));
        thisUnitPlayer.NotifyObservers(new ResourceChangeEvent(thisUnitPlayer, (int)-currentPart.Cost.y, ResourceType.METAL));
    }

    /// <summary>
    /// End building in interrputed state.
    /// </summary>
    public void EndBuildingInterrputed()
    {
        Player thisUnitPlayer = thisUnit.GetPlayer();
        thisUnitPlayer.NotifyObservers(new ResourceChangeEvent(thisUnitPlayer, (int) currentPart.Cost.x, ResourceType.SOLAR_POWER));
        thisUnitPlayer.NotifyObservers(new ResourceChangeEvent(thisUnitPlayer, (int) currentPart.Cost.y, ResourceType.METAL));
        currentActionTime = 0f;
        NeedsUpdate = false;
        thisUnit.NotifyObservers(new UnitBuildingInterruptedEvent(thisUnit, currentPart));
    }

    /// <summary>
    /// End building normally.
    /// </summary>
    /// <param name="createdUnit">Unit that was created.</param>
    public void EndBuildingNormal(Unit createdUnit)
    {
        NeedsUpdate = false;
        thisUnit.NotifyObservers(new UnitBuildingFinishedEvent(thisUnit, currentPart));
        thisUnit.State = UnitState.IDLE;
        currentActionTime = 0f;
        thisUnit.NotifyObservers(new CommandExecutedEvent(thisUnit.Commander));
    }

    /// <summary>
    /// Get unit prefab from resources folder.
    /// </summary>
    /// <param name="type"></param>
    /// <returns>Unit prefab if it is found - otherwise null.</returns>
    public GameObject GetUnitPrefab(UnitType type)
    {
        switch(type)
        {
            case UnitType.BATTLESHIP:
                return Resources.Load<GameObject>("BattleShip");
            case UnitType.DOCK:
                return Resources.Load<GameObject>("Dock");
            case UnitType.ENGINE:
                return Resources.Load<GameObject>("Engine");
            case UnitType.FIGHTER:
                return Resources.Load<GameObject>("Fighter");
            case UnitType.TOWER:
                return Resources.Load<GameObject>("Tower");
            case UnitType.WEAPON:
                return Resources.Load<GameObject>("Weapon");
            case UnitType.WORKER:
                return Resources.Load<GameObject>("Worker");
        }

        return null;
    }
}