﻿public class PauseCommand : PlayerCommand
{
    public override void Execute(Player player)
    {
        switch (Game.game.State)
        {
            case Game.GameState.IDLE:
                Game.game.State = Game.GameState.PAUSED;
                break;
            case Game.GameState.PAUSED:
                Game.game.State = Game.GameState.IDLE;
                break;
            default:
                break;
        }
        player.NotifyObservers(new PauseEvent(player));
    }
}
