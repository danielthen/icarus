﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player resources. Metal, solar power.
/// </summary>
public class PlayerResources
{
	public int Metal {get; set;}
	public int SolarPower {get; set;}
}
