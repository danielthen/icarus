﻿using UnityEngine;

public class BuildablePart : UnitCreationPart
{
    private Animator animator;
    public Unit Cityship { get; set; }
    private bool hasBuilding;
    public bool HasBuilding { get { return hasBuilding; } set { if (value) animator.SetBool("show", false); hasBuilding = value; } }

    void Start()
    {
        HasBuilding = false;
        animator = GetComponent<Animator>();
        Cityship = GetComponentInParent<Unit>();

        for (int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).tag == "BuildingLocation")
            {
                BuildingLocation = transform.GetChild(i);
                break;
            }
        }
    }

    void OnMouseEnter()
    {
        Player humanPlayer = Game.game.HumanPlayer;

        humanPlayer.NotifyObservers(new MouseOverEvent(humanPlayer, this, GetCorrectFeedBack()));

        if (!HasBuilding && humanPlayer.Units.HasUnitWithSpecificTypeSelected(UnitType.CITYSHIP))
        {
            animator.SetBool("show", true);
            Cityship.NotifyObservers(new BuildablePartMouseOverEvent(this, Cityship.GetComponentInChildren<UnitBuildingPanel>()));
        }
    }

    void OnMouseExit()
    {
        Player humanPlayer = Game.game.HumanPlayer;
        humanPlayer.NotifyObservers(new MouseOverExitEvent(humanPlayer, this));


        if (!HasBuilding && humanPlayer.Units.HasUnitWithSpecificTypeSelected(UnitType.CITYSHIP))
        {
            animator.SetBool("show", false);
            Cityship.NotifyObservers(new BuildablePartOnMouseExitEvent(this, Cityship.GetComponentInChildren<UnitBuildingPanel>()));
        }
    }

    /// <summary>
    /// Feedback for mouse over event.
    /// </summary>
    /// <returns>Feedback</returns>
    private string GetCorrectFeedBack()
    {
        switch(Type)
        {
            case UnitType.DOCK:
                return "You can build a DOCK here that let's you TRAIN units.";
            case UnitType.ENGINE:
                return "You can build an ENGINE here that makes your ship FASTER.";
            case UnitType.TOWER:
                return "You can build a TOWER here that gives your ship the ability to MOVE.";
            case UnitType.WEAPON:
                return "You can build a WEAPON here that attacks enemies.";
        }

        return "";
    }
}
