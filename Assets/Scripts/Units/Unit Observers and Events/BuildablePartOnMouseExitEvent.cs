﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildablePartOnMouseExitEvent : SubjectEvent
{
    public BuildablePart EventSender { get; set; }
    public UnitBuildingPanel BuildingPanel { get; set; }

    public BuildablePartOnMouseExitEvent(BuildablePart eventSender, UnitBuildingPanel buildingPanel)
    {
        EventSender = eventSender;
        BuildingPanel = buildingPanel;
    }
}
